/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.command;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Command processor utilities
 */
public final class CommandProcessor {

    private final static String EMPTY_STR = "";

    private CommandProcessor() {}

    /**
     * Get a command name
     *
     * @param command the command
     * @return the command name
     * @throws NullPointerException if the command is null
     */
    @Contract("null -> fail")
    @NotNull
    public static String getCommandName(final String command) throws NullPointerException {
        if (command == null)
            throw new NullPointerException("Command cannot be null");

        String target = parseCommand(command);
        if (target == null) return EMPTY_STR;

        return extractCommandNameFromFullCommand(target);
    }

    /**
     * Get the plugin executor of a command
     *
     * @return the plugin command
     * executor
     * @throws NullPointerException if the command is null
     */
    @Contract("null -> fail")
    @Nullable
    public static String getCommandPlugin(final String command) throws NullPointerException {
        if (command == null)
            throw new NullPointerException("Command cannot be null");

        String target = parseCommand(command);
        if (target == null) return EMPTY_STR;

        return extractPluginNameFromFullCommand(command);
    }

    private static @Nullable String parseCommand(String command) {
        String target = noSlashedCommand(command);

        target = parseCommandFromFullCommand(target);
        return target;
    }

    private static @Nullable String parseCommandFromFullCommand(String target) {
        if (target.contains(" ")) {
            String[] data = target.split(" ");
            String notEmpty = null;

            for (String str : data) {
                if (str.isEmpty()) continue;

                notEmpty = str;
                break;
            }

            if (notEmpty == null) return null;
            target = notEmpty;
        }

        return target;
    }

    private static String noSlashedCommand(final String command) {
        String target = command;
        if (target.startsWith("/"))
            target = target.substring(1);

        return target;
    }

    @NotNull
    private static String extractCommandNameFromFullCommand(final String target) {
        if (!target.contains(":"))
            return target.toLowerCase();

        String[] tData = target.split(":");
        if (tData.length < 2)
            return target.toLowerCase();

        if (tData[0].isEmpty() || tData[1].isEmpty())
            return target.toLowerCase();

        StringBuilder builder = new StringBuilder();
        for (int i = 1; i < tData.length; i++) {
            builder.append(tData[i]).append(':');
        }

        return builder.substring(0, builder.length() - 1)
                .toLowerCase();
    }

    @Nullable
    private static String extractPluginNameFromFullCommand(final String target) {
        if (!target.contains(":"))
            return null;

        String[] tData = target.split(":");
        if (tData.length < 2)
            return null;

        if (tData[0].isEmpty() || tData[1].isEmpty())
            return null;

        return tData[0];
    }
}

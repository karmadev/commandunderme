/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.command;

import es.karmadev.commandunderme.api.plugin.PluginWrapper;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;

/**
 * Represents a protected command
 */
public interface ProtectedCommand {

    /**
     * Get the command plugin. If the
     * command plugin is null, it means
     * it's a server command, which can
     * specifically be checked with
     * {@link #isBukkit()}
     *
     * @return the plugin of
     * the command
     */
    @NotNull
    Optional<? extends PluginWrapper<?>> getPlugin();

    /**
     * Get the protected command
     * name
     *
     * @return the command name
     */
    @NotNull
    String getName();

    /**
     * Get the raw command
     *
     * @return the raw command
     * form
     */
    default String getRaw() {
        Optional<? extends PluginWrapper<?>> plugin = this.getPlugin();
        return plugin.map(value ->
                String.format("%s:%s", value.getName(), this.getName()))
                .orElseGet(this::getName);

    }

    /**
     * Get if the command is a
     * bukkit command
     *
     * @return if the command is
     * provided by bukkit
     */
    boolean isBukkit();

    /**
     * Tries to evaluate the command
     *
     * @param command the command trying to
     *                evaluate
     * @return if the command evaluate
     */
    @Contract("null -> false")
    default boolean evaluates(final String command) {
        if (command == null)
            return false;

        final String source = CommandProcessor.getCommandName(command);
        final String target = this.getName();

        final String srcPlugin = CommandProcessor.getCommandPlugin(command);
        final PluginWrapper<?> trgPlugin = this.getPlugin().orElse(null);

        if (trgPlugin != null && srcPlugin == null)
            return false;

        if (srcPlugin != null && trgPlugin != null &&
                !trgPlugin.getName().equalsIgnoreCase(srcPlugin))
            return false;

        return source.equalsIgnoreCase(target);
    }
}
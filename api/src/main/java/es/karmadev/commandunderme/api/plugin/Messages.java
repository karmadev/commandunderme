/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.plugin;

/**
 * Represents the plugin messages
 */
public interface Messages {

    /**
     * Reload the messages
     * file
     */
    void reload();

    /**
     * Get the messages prefix
     *
     * @return the prefix
     */
    String getPrefix();

    /**
     * Get the missing permission
     * message
     *
     * @param permission the permission
     * @return the missing permission
     * message
     */
    String permission(final String permission);

    /**
     * Get the missing execution
     * permission message
     *
     * @return the missing permission
     * message
     */
    String executionPermission();

    /**
     * Get the type password in chat
     * message. Only in bungeecord
     *
     * @return type password in chat
     * message
     */
    String passwordInChat();

    /**
     * Get the invalid password
     * message
     *
     * @return the invalid password
     * message
     */
    String invalidPassword();

    /**
     * Get the successfully typed
     * password message
     *
     * @return the valid password
     * message
     */
    String runCommandAgain();

    /**
     * Get the successfully executed
     * command broadcast message
     *
     * @param player the player who executed
     *               the command
     * @param command the command
     * @return the success broadcast
     * message
     */
    String successBroadcast(final String player, final String command);

    /**
     * Get the failed executed
     * command broadcast message
     *
     * @param player the player who executed
     *               the command
     * @param command the command
     * @return the failed broadcast
     * message
     */
    String failureBroadcast(final String player, final String command);

    /**
     * Get the plugin reload
     * success message
     *
     * @return the reload success
     * message
     */
    String reloadSuccess();

    /**
     * Get the plugin reload
     * failure message
     *
     * @return the reload failure
     * message
     */
    String reloadFailure();

    /**
     * Get the plugin reload
     * usage message
     *
     * @return the reload usage
     * message
     */
    String reloadUsage();

    /**
     * Get the plugin setup
     * success message
     *
     * @return the setup success
     * message
     */
    String setupSuccess();

    /**
     * Get the plugin setup
     * failure message
     *
     * @return the setup failure
     * message
     */
    String setupFailure();

    /**
     * Get the plugin setup
     * usage message
     *
     * @return the setup usage
     * message
     */
    String setupUsage();

    /**
     * Get the plugin password
     * change success message
     *
     * @return the password change
     * success message
     */
    String passwordSuccess();

    /**
     * Get the plugin password
     * change failure message
     *
     * @return the password change
     * failure message
     */
    String passwordFailure();

    /**
     * Get the plugin change
     * password usage message
     *
     * @return the change password
     * usage message
     */
    String passwordUsage();

    /**
     * Get the plugin history player
     * not found message
     *
     * @param player the player
     * @return the player not found message
     */
    String historyNotFound(final String player);

    /**
     * Get the plugin history player
     * access denied message
     *
     * @param player the player
     * @return the access denied error
     */
    String historyAccessDenied(final String player);

    /**
     * Get the plugin command history
     * usage message
     *
     * @return the command history
     * usage message
     */
    String historyUsage();
}

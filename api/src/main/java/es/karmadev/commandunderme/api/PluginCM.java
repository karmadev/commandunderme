/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api;

import es.karmadev.commandunderme.api.client.Client;
import es.karmadev.commandunderme.api.command.ProtectedCommand;
import es.karmadev.commandunderme.api.plugin.Config;
import es.karmadev.commandunderme.api.plugin.Messages;
import es.karmadev.commandunderme.api.plugin.PluginWrapper;

import java.nio.file.Path;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

public abstract class PluginCM {

    private static PluginCM instance;

    protected void register() {
        if (instance != null)
            throw new SecurityException("Cannot re-define PluginCM");

        instance = this;
    }

    /**
     * Get the protected commands
     *
     * @return the protected commands
     */
    public abstract Collection<? extends ProtectedCommand> getCommands();

    /**
     * Get a protected command by a
     * plugin and a command
     *
     * @param plugin the plugin
     * @param command the command
     * @return the protected command
     */
    public Optional<? extends ProtectedCommand> getCommand(final PluginWrapper<?> plugin, final String command) {
        return this.getCommands().stream().filter((cmd) -> cmd.getPlugin().orElse(null) == plugin &&
                cmd.getName().equalsIgnoreCase(command)).findAny();
    }

    /**
     * Get all the stored clients
     *
     * @return the stored
     * clients
     */
    public abstract Collection<? extends Client<?>> getClients();

    /**
     * Get a client
     *
     * @param uniqueId the client unique id
     * @return the client
     */
    public abstract Optional<? extends Client<?>> getClient(final UUID uniqueId);

    /**
     * Get the plugin configuration
     *
     * @return the configuration
     */
    public abstract Config getConfiguration();

    /**
     * Get the plugin messages
     *
     * @return the messages
     */
    public abstract Messages getMessages();

    /**
     * Get the plugin data folder
     *
     * @return the plugin data folder
     */
    public abstract Path getDataFolder();

    /**
     * Extract a resource
     *
     * @param resourceName the resource to
     *                     extract
     * @return if the resource was extracted
     */
    public abstract boolean extractResource(final String resourceName);

    /**
     * Get the plugin cm instance
     *
     * @return the plugin cm
     * @throws EarlyException if the plugin cm is
     * not yet ready
     */
    public static PluginCM getInstance() throws EarlyException {
        return instance;
    }
}
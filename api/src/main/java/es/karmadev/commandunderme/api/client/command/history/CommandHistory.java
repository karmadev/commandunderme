/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.client.command.history;

import es.karmadev.commandunderme.api.client.command.history.filter.HistoryFilter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Iterator;

/**
 * Represents a client command history
 */
public interface CommandHistory extends Iterable<HistoryEntry> {

    /**
     * Get all the history entries
     *
     * @param filters the history filters
     * @return the history entries
     */
    Collection<HistoryEntry> getEntries(final @Nullable HistoryFilter... filters);

    /**
     * Add an entry to the
     * history
     *
     * @param entry the entry to add
     */
    <T extends HistoryEntry> void addEntry(final T entry);

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @NotNull
    @Override
    default Iterator<HistoryEntry> iterator() {
        return this.getEntries().iterator();
    }
}

/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.client.command.history.filter;

import es.karmadev.commandunderme.api.client.command.history.HistoryEntry;
import org.jetbrains.annotations.NotNull;

import java.time.Instant;
import java.time.ZoneId;

/**
 * Timestamp filter
 */
public final class TimestampFilter implements HistoryFilter {

    private final Instant from;
    private final Instant to;

    private TimestampFilter(final Instant from, final Instant to) {
        if (from == null && to == null)
            throw new IllegalArgumentException("Cannot create timestamp filter for start and end null dates");

        this.from = from;
        this.to = to;
    }


    /**
     * Get if the entry evaluates
     * to the filter
     *
     * @param entry the entry
     * @return if evaluates
     */
    @Override
    public boolean evaluates(final HistoryEntry entry) {
        if (entry == null)
            return false;

        final Instant entryExec = entry.getExecutionDate();
        return entryExec.isAfter(this.from) && entryExec.isBefore(this.to);
    }

    /**
     * Create a timestamp filter
     * builder
     *
     * @return the filter builder
     */
    @NotNull
    public static FilterBuilder builder() {
        return new FilterBuilder();
    }

    /**
     * Timestamp filter builder
     */
    public static class FilterBuilder {

        private Instant from = Instant.MIN;
        private Instant to = Instant.now()
                .atZone(ZoneId.systemDefault())
                .toLocalDate()
                .atTime(23, 59, 59, 999)
                .atZone(ZoneId.systemDefault())
                .toInstant();

        private FilterBuilder() {}

        /**
         * Set the filter to filter entries
         * made NOT before (after) the specified
         * moment
         *
         * @param instant the moment
         * @return the builder
         */
        @NotNull
        public FilterBuilder to(final Instant instant) {
            if (to == null)
                return this;

            this.to = instant;
            return this;
        }

        /**
         * Set the filter to filter entries
         * made NOT after (before) the specified
         * moment
         *
         * @param instant the moment
         * @return the builder
         */
        @NotNull
        public FilterBuilder from(final Instant instant) {
            if (instant == null)
                return this;

            this.from = instant;
            return this;
        }

        /**
         * Build the filter
         *
         * @return the filter
         */
        @NotNull
        public TimestampFilter build() {
            Instant finalFrom = this.from;
            Instant finalTo = this.to;

            if (finalFrom.isAfter(finalTo)) {
                Instant tmp = finalTo;
                finalTo = finalFrom;
                finalFrom = tmp;
            }

            return new TimestampFilter(finalFrom, finalTo);
        }
    }
}
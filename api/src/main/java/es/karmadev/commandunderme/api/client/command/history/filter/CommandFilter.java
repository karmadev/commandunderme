/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.client.command.history.filter;

import es.karmadev.commandunderme.api.client.command.history.HistoryEntry;
import org.jetbrains.annotations.NotNull;

/**
 * Command filter
 */
public final class CommandFilter implements HistoryFilter {

    private final String command;
    private final String plugin;

    private CommandFilter(final String command, final String plugin) {
        this.command = command;
        this.plugin = plugin;
    }

    /**
     * Get if the entry evaluates
     * to the filter
     *
     * @param entry the entry
     * @return if evaluates
     */
    @Override
    public boolean evaluates(final HistoryEntry entry) {
        if (entry == null)
            return false;

        final String cmdName = entry.getCommand();
        final String bplName = entry.getCommandPlugin();

        if (this.command == null)
            return this.plugin.equalsIgnoreCase(bplName);

        if (this.plugin == null)
            return this.command.equalsIgnoreCase(cmdName);

        return this.plugin.equalsIgnoreCase(bplName) &&
                this.command.equalsIgnoreCase(cmdName);
    }

    /**
     * Create a timestamp filter
     * builder
     *
     * @return the filter builder
     */
    @NotNull
    public static FilterBuilder builder() {
        return new FilterBuilder();
    }

    /**
     * Timestamp filter builder
     */
    public static class FilterBuilder {

        private String command;
        private String plugin;

        private FilterBuilder() {}

        /**
         * Set the filter to filter entries
         * for the specified command
         *
         * @param command the command
         * @return the builder
         */
        @NotNull
        public FilterBuilder forCommand(final String command) {
            this.command = command;
            return this;
        }

        /**
         * Set the filter to filter entries
         * for the specified plugin
         *
         * @param plugin the plugin
         * @return the builder
         */
        @NotNull
        public FilterBuilder forPlugin(final String plugin) {
            this.plugin = plugin;
            return this;
        }

        /**
         * Build the filter
         *
         * @return the filter
         */
        @NotNull
        public CommandFilter build() {
            if (this.command == null && this.plugin == null)
                throw new NullPointerException("Cannot create command filter for null command and plugin!");

            return new CommandFilter(this.command, this.plugin);
        }
    }
}
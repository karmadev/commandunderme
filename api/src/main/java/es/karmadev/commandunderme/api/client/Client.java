/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.client;

import es.karmadev.commandunderme.api.client.command.CommandTicket;
import es.karmadev.commandunderme.api.client.command.history.CommandHistory;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

/**
 * Represents the basic interface
 * for a client implementation. The
 * client can receive a message, be
 * kicked or update/modify its current
 * command ticket
 */
public interface Client<T> {

    /**
     * Get the client name
     *
     * @return the client name
     */
    @NotNull
    String getName();

    /**
     * Get the client unique id
     *
     * @return the unique ID
     */
    @NotNull
    UUID getUniqueId();

    /**
     * Get the current client command
     * token. The only way a client
     * might not have a ticket is when
     * he joins the game
     *
     * @return the command ticket
     */
    @NotNull
    Optional<CommandTicket> getCommandTicket();

    /**
     * Get the client command
     * history
     *
     * @return the command history
     */
    @NotNull
    CommandHistory getHistory();

    /**
     * Send a message to the client
     *
     * @param message the message to send
     * @param params the messages parameters
     * @throws NullPointerException if the message is null
     */
    @Contract("null, _ -> fail")
    void sendMessage(final String message, final Object... params) throws NullPointerException;

    /**
     * Sends all the messages to the
     * client
     *
     * @param messages the messages to send
     * @throws NullPointerException if the messages are null
     */
    @Contract("null -> fail")
    default void sendMessage(final String[] messages) throws NullPointerException {
        if (messages == null)
            throw new NullPointerException("Messages cannot be null");

        for (String str : messages) {
            if (str == null)
                str = "";

            this.sendMessage(str);
        }
    }

    /**
     * Kick the client
     *
     * @param reason the kick reason
     * @param params the message parameters
     * @throws NullPointerException if the kick reason is null
     */
    @Contract("null, _ -> fail")
    void kick(final String reason, final Object... params) throws NullPointerException;

    /**
     * Tries to obtain the online
     * player of the client
     *
     * @return the client
     */
    @Nullable
    T getPlayer();

    /**
     * Get if the client is currently
     * online
     *
     * @return if the client is online
     */
    boolean isOffline();

    /**
     * Save the client
     * @throws IOException if the file fails to save
     */
    void save() throws IOException;

    /**
     * Get if the client can access
     * other client's history. This method
     * only works if both clients are
     * online
     *
     * @param other the other client
     * @return if the client can access the
     * other client history
     * @throws NullPointerException if the client to check
     * with is null
     */
    @Contract("null -> fail")
    boolean canAccess(final Client<T> other) throws NullPointerException;
}

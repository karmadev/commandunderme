/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.client.command.history;

import es.karmadev.commandunderme.api.plugin.ProtectionMode;
import org.jetbrains.annotations.NotNull;

import java.time.Instant;

/**
 * Represents a client history entry
 * in the {@link CommandHistory}
 */
public interface HistoryEntry {

    /**
     * Get when the command was
     * executed
     *
     * @return the command execution
     * date
     */
    @NotNull
    Instant getExecutionDate();

    /**
     * Get when the command execution
     * was validated. Useful when the
     * execution mode is {@link ProtectionMode#CHAINED}
     * or {@link ProtectionMode#TRUSTED}
     *
     * @return the validation date
     */
    @NotNull
    Instant getValidatedDate();

    /**
     * Get the executed command
     *
     * @return the command that
     * got executed
     */
    @NotNull
    String getCommand();

    /**
     * Get the command plugin. Please
     * note if the command is a bukkit
     * or minecraft, this will return
     * bukkit:server or minecraft:server
     *
     * @return the plugin
     */
    @NotNull
    String getCommandPlugin();

    /**
     * Get if the command was
     * successfully executed
     *
     * @return if the command was
     * success
     */
    boolean wasSuccess();

    /**
     * Get the executed command
     * arguments. If no arguments
     * were run, this will simply
     * return an empty array
     *
     * @return the arguments of
     * the command
     */
    @NotNull
    String[] getArguments();
}

/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.plugin;

import java.util.Collection;

/**
 * Represents the plugin
 * configuration
 */
public interface Config {

    /**
     * Reload the configuration
     * file
     */
    void reload();

    /**
     * Get a list containing all
     * the blocked commands
     *
     * @return the blocked
     * commands
     */
    Collection<String> getBlockedCommands();

    /**
     * Get a list containing all
     * the hidden commands
     *
     * @return the hidden
     * commands
     */
    Collection<String> getHiddenCommands();

    /**
     * Get the command protection
     * mode
     *
     * @return the protection mode
     */
    ProtectionMode getProtectionMode();

    /**
     * Get the amount of seconds a
     * client is allowed to run commands
     * without authenticating when
     * in chain mode
     *
     * @return the chain time
     */
    int getChainTime();
}

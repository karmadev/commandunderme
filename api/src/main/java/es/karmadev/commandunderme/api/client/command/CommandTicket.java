/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.client.command;

import es.karmadev.commandunderme.api.client.command.exception.ResultCompletedException;
import es.karmadev.commandunderme.api.client.command.exception.ResultUncompletedException;
import es.karmadev.commandunderme.api.command.ProtectedCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Instant;

/**
 * Represents a client command
 * ticket
 */
public final class CommandTicket {

    private final ProtectedCommand command;

    private boolean isDefined = false;
    private boolean success = false;

    private Instant resultTime;

    private boolean isConsumed = false;

    /**
     * Initializes the client command
     * ticket
     *
     * @param command the command ticket
     */
    public CommandTicket(final ProtectedCommand command) {
        this.command = command;
    }

    /**
     * Get the ticket command
     *
     * @return the command
     */
    @NotNull
    public ProtectedCommand getCommand() {
        return this.command;
    }

    /**
     * Set the result of the
     * ticket
     *
     * @param status the status
     * @throws ResultCompletedException if the result has been
     * already set
     */
    public void setResult(final boolean status) throws ResultCompletedException {
        if (this.isDefined)
            throw new ResultCompletedException();

        this.isDefined = true;
        this.success = status;
        this.resultTime = Instant.now();
    }

    /**
     * Get if the ticket has
     * been yet defined
     *
     * @return if the ticket has
     * been defined
     */
    public boolean isDefined() {
        return this.isDefined;
    }

    /**
     * Get the success state for the
     * command ticket
     *
     * @return the success state
     * @throws ResultUncompletedException if the ticket has
     * not been yet completed
     */
    public boolean isSuccess() throws ResultUncompletedException {
        if (!this.isDefined)
            throw new ResultUncompletedException();

        return this.success;
    }

    /**
     * Get the ticket result
     *
     * @return when the ticket result was
     * set
     * @throws ResultUncompletedException if the ticket has
     * not been yet completed
     */
    @Nullable
    public Instant getResultTime() throws ResultUncompletedException {
        return this.resultTime;
    }

    /**
     * Get if the ticket has been
     * consumed
     *
     * @return if the ticket is
     * consumed
     */
    public boolean isConsumed() {
        return this.isConsumed;
    }

    /**
     * Consume the ticket
     */
    public void consume() {
        this.isConsumed = true;
    }

    /**
     * Clones this ticket for the
     * new command
     *
     * @param command the command
     * @param renew renew the time
     * @return the ticket
     */
    public CommandTicket atCommand(final ProtectedCommand command, final boolean renew) {
        CommandTicket ticket = new CommandTicket(command);
        ticket.isDefined = this.isDefined;
        ticket.success = this.success;
        ticket.resultTime = (this.isDefined ? (renew ? Instant.now() : this.resultTime) : null);

        return ticket;
    }
}

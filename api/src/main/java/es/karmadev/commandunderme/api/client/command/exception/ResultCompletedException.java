/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.client.command.exception;

import es.karmadev.commandunderme.api.client.command.CommandTicket;

/**
 * This exception is fired when a
 * call to {@link CommandTicket#setResult(boolean)} is made
 * but the ticket has been already
 * completed
 */
public class ResultCompletedException extends Exception {
}

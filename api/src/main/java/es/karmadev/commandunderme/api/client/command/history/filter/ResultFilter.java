/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.client.command.history.filter;

import es.karmadev.commandunderme.api.client.command.history.HistoryEntry;

/**
 * Execution result filter
 */
public final class ResultFilter implements HistoryFilter {

    /**
     * Successfully executed
     * command
     */
    public final static ResultFilter SUCCESS = new ResultFilter(true);
    /**
     * Failed executed
     * command
     */
    public final static ResultFilter FAILED = new ResultFilter(false);

    private final boolean result;

    private ResultFilter(final boolean result) {
        this.result = result;
    }


    /**
     * Get if the entry evaluates
     * to the filter
     *
     * @param entry the entry
     * @return if evaluates
     */
    @Override
    public boolean evaluates(final HistoryEntry entry) {
        if (entry == null)
            return false;

        return entry.wasSuccess() == this.result;
    }
}
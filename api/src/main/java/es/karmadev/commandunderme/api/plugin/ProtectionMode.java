/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.plugin;

/**
 * Represents the plugin command protection
 * mode
 */
public enum ProtectionMode {
    /**
     * Every new command execution requires to write
     * the access password
     */
    STRICT,
    /**
     * The password is required only once
     */
    TRUSTED,
    /**
     * The password is required only once, but if the client
     * stops executing "protected" commands, he will be asked
     * to type the password again when he runs a protected
     * command again
     */
    CHAINED
}

/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.event.bukkit.command;

import es.karmadev.commandunderme.api.client.Client;
import es.karmadev.commandunderme.api.command.ProtectedCommand;
import es.karmadev.commandunderme.api.event.bukkit.CMEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

/**
 * This event is fired when a command
 * is executed successfully by a client
 */
public class CommandSuccessEvent extends CMEvent implements Cancellable {

    private final static HandlerList HANDLER_LIST = new HandlerList();

    private final Client<Player> client;
    private boolean cancelled;

    /**
     * Initialize the event
     *
     * @param client the client who executed the
     *               command
     * @param command the event command
     */
    public CommandSuccessEvent(final Client<Player> client, final ProtectedCommand command) {
        super(command);
        this.client = client;
    }

    /**
     * Get the client who executed
     * the command
     *
     * @return the client
     */
    public Client<Player> getClient() {
        return this.client;
    }

    /**
     * Gets the cancellation state of this event. A cancelled event will not
     * be executed in the server, but will still pass to other plugins
     *
     * @return true if this event is cancelled
     */
    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    /**
     * Sets the cancellation state of this event. A cancelled event will not
     * be executed in the server, but will still pass to other plugins.
     *
     * @param cancel true if you wish to cancel this event
     */
    @Override
    public void setCancelled(final boolean cancel) {
        this.cancelled = cancel;
    }

    @NotNull
    @Override
    public HandlerList getHandlers() {
        return HANDLER_LIST;
    }

    public static HandlerList getHandlerList() {
        return HANDLER_LIST;
    }
}

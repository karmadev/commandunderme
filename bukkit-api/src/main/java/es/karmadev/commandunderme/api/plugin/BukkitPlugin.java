/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.plugin;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.util.Optional;

public class BukkitPlugin extends PluginWrapper<Plugin> {

    protected BukkitPlugin(final String name) {
        super(name);
    }

    /**
     * Get the plugin
     *
     * @return the plugin
     */
    @Override
    public Plugin getPlugin() {
        return Bukkit.getPluginManager().getPlugin(this.name);
    }

    /**
     * Wrap a bukkit plugin
     * by name
     *
     * @param name the plugin name
     * @return the wrapped plugin
     */
    public static BukkitPlugin wrapRaw(final String name) {
        return new BukkitPlugin(name);
    }

    /**
     * Wrap a bukkit plugin
     *
     * @param plugin the plugin to wrap
     * @return the wrapped plugin
     */
    public static Optional<BukkitPlugin> wrap(final Plugin plugin) {
        if (plugin == null) {
            return Optional.empty();
        }

        return Optional.of(new BukkitPlugin(plugin.getName()));
    }
}

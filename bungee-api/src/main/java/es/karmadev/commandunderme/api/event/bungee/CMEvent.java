/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.event.bungee;

import es.karmadev.commandunderme.api.command.ProtectedCommand;
import net.md_5.bungee.api.plugin.Event;

/**
 * Represents a generic command
 * under me event
 */
public abstract class CMEvent extends Event {

    private final ProtectedCommand command;

    /**
     * Initialize the event
     *
     * @param command the event command
     */
    public CMEvent(final ProtectedCommand command) {
        this.command = command;
    }

    /**
     * Get the command that has tried
     * to be run
     *
     * @return the command
     */
    public ProtectedCommand getCommand() {
        return this.command;
    }
}

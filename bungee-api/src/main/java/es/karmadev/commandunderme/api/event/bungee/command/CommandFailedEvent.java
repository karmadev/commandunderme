/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.event.bungee.command;

import es.karmadev.commandunderme.api.client.Client;
import es.karmadev.commandunderme.api.command.ProtectedCommand;
import es.karmadev.commandunderme.api.event.bungee.CMEvent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

/**
 * This event is fired when a command
 * is NOT executed successfully by a client
 */
public class CommandFailedEvent extends CMEvent {

    private final Client<ProxiedPlayer> client;

    /**
     * Initialize the event
     *
     * @param client the client who executed the
     *               command
     * @param command the event command
     */
    public CommandFailedEvent(final Client<ProxiedPlayer> client, final ProtectedCommand command) {
        super(command);
        this.client = client;
    }

    /**
     * Get the client who executed
     * the command
     *
     * @return the client
     */
    public Client<ProxiedPlayer> getClient() {
        return this.client;
    }
}

/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.bungee.command.sub;

import es.karmadev.commandunderme.api.plugin.Messages;
import es.karmadev.commandunderme.api.util.Utils;
import es.karmadev.commandunderme.plugin.bungee.CMPlugin;
import es.karmadev.commandunderme.plugin.bungee.command.sub.executors.*;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;

public final class CommandAnalyzer {

    private final CMPlugin plugin;

    private final HelpExecutor helpExecutor;
    private final ReloadExecutor reloadExecutor;
    private final SetupExecutor setupExecutor;
    private final PasswordExecutor passwordExecutor;
    private final HistoryExecutor historyExecutor;

    public CommandAnalyzer(final CMPlugin plugin) {
        this.plugin = plugin;

        this.helpExecutor = new HelpExecutor(plugin);
        this.reloadExecutor = new ReloadExecutor(this, plugin);
        this.setupExecutor = new SetupExecutor(plugin);
        this.passwordExecutor = new PasswordExecutor(plugin);
        this.historyExecutor = new HistoryExecutor(plugin);
    }

    public void analyze(final CommandSender sender, final String command, final String[] args) {
        final Messages messages = this.plugin.getProvider().getMessages();

        switch (command) {
            case "reload":
                this.reloadExecutor.run(sender);
                break;
            case "setup":
                if (args.length == 0) {
                    sender.sendMessage(
                            TextComponent.fromLegacy(
                                    Utils.colorize(
                                            messages.setupUsage(),
                                            true
                                    )
                            )
                    );
                    return;
                }

                this.setupExecutor.run(sender, args[0]);
                break;
            case "pwd":
                if (args.length < 2) {
                    sender.sendMessage(
                            TextComponent.fromLegacy(
                                    Utils.colorize(
                                            messages.passwordUsage(),
                                            true
                                    )
                            )
                    );
                    return;
                }

                this.passwordExecutor.run(sender, args[0], args[1]);
                break;
            case "history":
                if (args.length == 0) {
                    sender.sendMessage(
                            TextComponent.fromLegacy(
                                    Utils.colorize(
                                            messages.historyUsage(),
                                            true
                                    )
                            )
                    );
                    return;
                }

                this.historyExecutor.run(sender, args);
                break;
            case "help":
            default:
                helpExecutor.run(sender);
        }
    }

    public void refreshHelpMessage() {
        this.helpExecutor.refreshHelpMessage();
    }
}

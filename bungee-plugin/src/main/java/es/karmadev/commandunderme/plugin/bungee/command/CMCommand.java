/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.bungee.command;

import es.karmadev.commandunderme.plugin.bungee.CMPlugin;
import es.karmadev.commandunderme.plugin.bungee.command.sub.CommandAnalyzer;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

import java.util.Arrays;

public class CMCommand extends Command {

    private final CommandAnalyzer analyzer;

    public CMCommand(final CMPlugin plugin) {
        super("cm");
        this.analyzer = new CommandAnalyzer(plugin);
    }

    /**
     * Execute this command with the specified sender and arguments.
     *
     * @param sender the executor of this command
     * @param args   arguments used to invoke this command
     */
    @Override
    public void execute(final CommandSender sender, final String[] args) {
        if (args.length == 0) {
            this.analyzer.analyze(sender, "help", new String[0]);
        } else {
            this.analyzer.analyze(sender, args[0].toLowerCase(), Arrays.copyOfRange(args, 1, args.length));
        }
    }
}
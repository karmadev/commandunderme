/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.bungee.listener;

import es.karmadev.commandunderme.api.client.command.CommandTicket;
import es.karmadev.commandunderme.api.command.CommandProcessor;
import es.karmadev.commandunderme.api.plugin.Config;
import es.karmadev.commandunderme.plugin.bungee.CMPlugin;
import es.karmadev.commandunderme.plugin.bungee.impl.client.CMClient;
import net.md_5.bungee.api.connection.Connection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.TabCompleteEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.time.Instant;
import java.util.Collection;

public class CompletionHandler implements Listener {

    private final CMPlugin plugin;

    public CompletionHandler(final CMPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onTabComplete(TabCompleteEvent e) {
        Connection sender = e.getReceiver();
        if (!(sender instanceof ProxiedPlayer))
            return;

        ProxiedPlayer player = (ProxiedPlayer) sender;
        CMClient client = plugin.getProvider().getClient(player.getUniqueId())
                .orElse(null);
        if (client == null) return;

        Config configuration = this.plugin.getProvider().getConfiguration();

        boolean block;

        CommandTicket ticket = client.getCommandTicket().orElse(null);
        switch (configuration.getProtectionMode()) {
            case STRICT:
                block = true;
                break;
            case TRUSTED:
                block = ticket == null || !ticket.isDefined() || !ticket.isSuccess();
                break;
            case CHAINED:
            default:
                block = ticket == null || !ticket.isDefined() || isOutChainTime(ticket, configuration);
                break;
        }

        if (!player.hasPermission("cm.execute") || block) {
            Collection<String> hidden = configuration.getHiddenCommands();

            String label = e.getCursor();
            String cmName = CommandProcessor.getCommandName(label);
            String cmPlugin = CommandProcessor.getCommandPlugin(label);
            for (String str : hidden) {
                String sName = CommandProcessor.getCommandName(str);
                String sPlugin = CommandProcessor.getCommandPlugin(str);

                if (evaluates(cmName, cmPlugin, sName, sPlugin))
                    e.getSuggestions().clear();
            }
        }

    }

    private boolean isOutChainTime(final CommandTicket currentTicket, final Config config) {
        Instant now = Instant.now();
        Instant last = currentTicket.getResultTime();
        return last == null || now.isAfter(last.plusSeconds(config.getChainTime()));
    }

    private boolean evaluates(final String source, final String srcPlugin,
                                        final String target, final String trgPlugin) {
        if (trgPlugin != null && srcPlugin == null)
            return false;

        if (srcPlugin != null && trgPlugin != null &&
                !trgPlugin.equalsIgnoreCase(srcPlugin))
            return false;

        return source.equalsIgnoreCase(target);
    }
}

/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.bungee.listener;

import es.karmadev.commandunderme.plugin.bungee.CMPlugin;
import es.karmadev.commandunderme.plugin.bungee.impl.client.CMClient;
import es.karmadev.commandunderme.plugin.bungee.impl.client.history.CMHistory;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.io.IOException;
import java.util.Collections;
import java.util.UUID;
import java.util.logging.Level;

public final class ConnectionHandler implements Listener {

    private final CMPlugin plugin;

    public ConnectionHandler(final CMPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onJoin(ServerConnectedEvent e) {
        ProxiedPlayer player = e.getPlayer();
        String name = player.getName();

        UUID uniqueId = UUID.nameUUIDFromBytes(("OfflinePlayer:" + name).getBytes());
        CMClient client = this.plugin.getProvider().getClient(uniqueId).orElse(null);

        if (client == null) {
            client = this.plugin.getProvider().getClients().stream()
                    .filter((cl) -> cl.getName().equals(name))
                    .findAny().orElse(null);
        }

        if (client == null) {
            client = new CMClient(name, uniqueId, new CMHistory(Collections.emptyList()));
            this.plugin.getProvider().addClient(client);

            try {
                client.save();
            } catch (IOException ex) {
                this.plugin.getLogger().log(Level.SEVERE, ex, () -> "Failed to store user " + name);
            }
        }

        client.load(player);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onQuit(PlayerDisconnectEvent e) {
        ProxiedPlayer player = e.getPlayer();
        CMClient client = this.plugin.getProvider().getClient(player.getUniqueId())
                .orElse(null);

        if (client == null)
            return;

        client.load(null);
    }
}

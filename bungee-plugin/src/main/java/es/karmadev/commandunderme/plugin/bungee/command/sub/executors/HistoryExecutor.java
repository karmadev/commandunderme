/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.bungee.command.sub.executors;

import es.karmadev.commandunderme.api.client.command.history.CommandHistory;
import es.karmadev.commandunderme.api.client.command.history.HistoryEntry;
import es.karmadev.commandunderme.api.client.command.history.filter.CommandFilter;
import es.karmadev.commandunderme.api.client.command.history.filter.HistoryFilter;
import es.karmadev.commandunderme.api.client.command.history.filter.ResultFilter;
import es.karmadev.commandunderme.api.client.command.history.filter.TimestampFilter;
import es.karmadev.commandunderme.api.command.CommandProcessor;
import es.karmadev.commandunderme.api.plugin.Messages;
import es.karmadev.commandunderme.api.util.Utils;
import es.karmadev.commandunderme.plugin.bungee.CMPlugin;
import es.karmadev.commandunderme.plugin.bungee.impl.client.CMClient;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.jetbrains.annotations.NotNull;

import java.time.Instant;
import java.util.*;

public class HistoryExecutor {

    private final CMPlugin plugin;

    public HistoryExecutor(final CMPlugin plugin) {
        this.plugin = plugin;
    }

    public void run(final CommandSender sender, final String[] args) {
        Messages messages = this.plugin.getProvider().getMessages();
        if (!sender.hasPermission("cm.history")) {
            sender.sendMessage(
                    TextComponent.fromLegacy(
                            Utils.colorize(messages.permission("cm.history"), true)
                    )
            );
            return;
        }

        String target = args[0];
        UUID uniqueId = parseID(target);
        CMClient client = this.plugin.getProvider().getClient(uniqueId).orElse(null);
        if (client == null) {
            ProxiedPlayer player = this.plugin.getProxy().getPlayer(target);
            if (player != null && player.isConnected()) {
                client = this.plugin.getProvider().getClient(player.getUniqueId()).orElse(null);
            }
        }

        if (client == null) {
            sender.sendMessage(
                    TextComponent.fromLegacy(
                            Utils.colorize(messages.historyNotFound(target), true)
                    )
            );
            return;
        }

        boolean hasAccess = true;
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer executor = (ProxiedPlayer) sender;
            CMClient executorClient = this.plugin.getProvider().getClient(executor.getUniqueId())
                    .orElseThrow(RuntimeException::new);

            hasAccess = executorClient.canAccess(client);
        }

        if (!hasAccess) {
            sender.sendMessage(
                   TextComponent.fromLegacy(
                           Utils.colorize(messages.historyAccessDenied(target),
                                   true)
                   )
            );
            return;
        }

        List<HistoryFilter> filters = parseFilters(sender, args);
        if (filters == null)
            return;

        CommandHistory history = client.getHistory();
        printHistoryToSender(sender, args, history, filters.toArray(new HistoryFilter[0]));
    }

    private List<HistoryFilter> parseFilters(final CommandSender sender, final String[] args) {
        Map<String, String[]> rawArguments = Utils.extractArguments(args);
        List<HistoryFilter> filters = new ArrayList<>();

        TimestampFilter.FilterBuilder builder = TimestampFilter.builder();
        if (readFromArgument(rawArguments, "from", true, sender, builder)) return null;
        if (readFromArgument(rawArguments, "to", false, sender, builder)) return null;
        readCommandArgument(rawArguments, filters);
        readResultArgument(rawArguments, filters);

        filters.add(builder.build());
        return filters;
    }

    private static void readResultArgument(Map<String, String[]> rawArguments, List<HistoryFilter> filters) {
        if (rawArguments.containsKey("result")) {
            String[] arguments = rawArguments.get("result");
            if (arguments != null && arguments.length > 0) {
                String result = arguments[0].toLowerCase();
                filters.add(result.equals("y") || result.equals("success") ||
                        result.equals("true") || result.equals("yes") ||
                        result.equals("executed") ? ResultFilter.SUCCESS : ResultFilter.FAILED);
            }
        }
    }

    private static void readCommandArgument(Map<String, String[]> rawArguments, List<HistoryFilter> filters) {
        if (rawArguments.containsKey("command")) {
            String[] arguments = rawArguments.get("command");
            for (String str : arguments) {
                try {
                    String cName = CommandProcessor.getCommandName(str);
                    String cPlugin = CommandProcessor.getCommandPlugin(str);

                    CommandFilter filter = CommandFilter.builder()
                            .forCommand(cName)
                            .forPlugin(cPlugin)
                            .build();
                    filters.add(filter);
                } catch (Throwable ignored) {}
            }
        }
    }

    private static boolean readFromArgument(Map<String, String[]> rawArguments, String from, boolean startOfDay, CommandSender sender,
                                            TimestampFilter.FilterBuilder builder) {
        if (rawArguments.containsKey(from)) {
            String[] content = rawArguments.get(from);
            if (content != null && content.length > 0) {
                String firstEntry = content[0];
                Instant instant = Utils.humanToInstant(firstEntry, startOfDay);

                if (instant == null) {
                    sender.sendMessage(
                            TextComponent.fromLegacy(
                                    Utils.colorize("&cInvalid time format, expected day/month/year or [a]month/day/year", true)
                            )
                    );
                    return true;
                }

                if (from.equals("from")) {
                    builder.from(instant);
                } else {
                    builder.to(instant);
                }
            }
        }
        return false;
    }

    private void printHistoryToSender(CommandSender sender, String[] args, CommandHistory history, final HistoryFilter[] filters) {
        int page = 1;
        if (args.length >= 2) {
            try {
                page = Math.max(1, Integer.parseInt(args[1]));
            } catch (NumberFormatException ignored) {}
        }

        HistoryEntry[] entryArray = history.getEntries(filters).toArray(new HistoryEntry[0]);
        int maxPage = entryArray.length / 5;
        if (maxPage < (page - 1)) {
            sender.sendMessage(
                   TextComponent.fromLegacy(
                           Utils.colorize("&cCannot read page&7 " + page + "&c. Maximum is:&7 " + maxPage, true)
                   )
            );
            return;
        }

        int indexStart = (page - 1) * 5;
        HistoryEntry[] entries = splitEntries(entryArray, indexStart);

        for (HistoryEntry entry : entries) {
            printEntry(entry, sender);
        }
        sender.sendMessage(TextComponent.fromLegacy(
                Utils.colorize("&7Page: &e" + page + "&8 | &6" + (maxPage + 1), false)
        ));
    }

    private void printEntry(final HistoryEntry entry, final CommandSender sender) {
        List<String> messages = buildEntryLore(entry);
        messages.forEach((message) -> sender.sendMessage(
                TextComponent.fromLegacy(
                        message
                )
        ));
    }

    private UUID parseID(final String target) {
        UUID full = fromTrimmed(target);
        if (full != null) return full;

        try {
            return UUID.fromString(target);
        } catch (IllegalArgumentException ignored) {}

        return UUID.nameUUIDFromBytes(("OfflinePlayer:" + target).getBytes());
    }

    private UUID fromTrimmed(final String str) {
        if (str.length() != 32)
            return null;

        String uuidWithHyphens = str.replaceFirst(
                "(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})",
                "$1-$2-$3-$4-$5"
        );
        return UUID.fromString(uuidWithHyphens);
    }

    private static @NotNull List<String> buildEntryLore(final HistoryEntry entry) {
        List<String> lore = new ArrayList<>();
        lore.add(Utils.colorize("&3&m---------------", false));
        lore.add("");
        lore.add(Utils.colorize("&7Plugin&8:&b " + entry.getCommandPlugin(), false));
        lore.add(Utils.colorize("&7Status&8:&b " + (entry.wasSuccess() ? "Success" : "Failed"), false));
        lore.add(Utils.colorize("&7Executed&8:&b " + Utils.instantToHuman(entry.getExecutionDate()), false));
        lore.add(Utils.colorize("&7Validation&8:&b " + Utils.instantToHuman(entry.getValidatedDate()), false));
        String[] arguments = entry.getArguments();
        if (arguments.length > 0) {
            lore.add(Utils.colorize("&7Arguments&8:", false));
            for (String argument : arguments) {
                lore.add(Utils.colorize(" &7-&b " + argument, false));
            }
        }

        return lore;
    }

    private HistoryEntry[] splitEntries(final HistoryEntry[] array, final int from) {
        if (array.length == 0 || from >= array.length) {
            return new HistoryEntry[0];
        }

        int length = Math.min(5, array.length - from);
        HistoryEntry[] result = new HistoryEntry[length];
        System.arraycopy(array, from, result, 0, length);

        return result;
    }
}

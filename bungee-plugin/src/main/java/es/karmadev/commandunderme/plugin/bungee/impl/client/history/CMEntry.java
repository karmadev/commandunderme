/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.bungee.impl.client.history;

import es.karmadev.api.kson.processor.JsonSerializable;
import es.karmadev.api.kson.processor.construct.JsonConstructor;
import es.karmadev.api.kson.processor.construct.JsonParameter;
import es.karmadev.api.kson.processor.conversor.InstantTransformer;
import es.karmadev.api.kson.processor.field.JsonElement;
import es.karmadev.api.kson.processor.field.Transformer;
import es.karmadev.commandunderme.api.client.command.history.HistoryEntry;
import es.karmadev.commandunderme.api.plugin.ProtectionMode;
import org.jetbrains.annotations.NotNull;

import java.time.Instant;

@JsonSerializable
public final class CMEntry implements HistoryEntry {

    @JsonElement(name = "execution")
    @Transformer(transformer = InstantTransformer.class)
    private final Instant executionDate;

    @JsonElement(name = "validation")
    @Transformer(transformer = InstantTransformer.class)
    private final Instant validationDate;

    @JsonElement(name = "command")
    private final String command;

    @JsonElement(name = "plugin")
    private final String plugin;

    @JsonElement(name = "successful")
    private final boolean wasSuccessful;

    @JsonElement(name = "arguments")
    private final String[] arguments;

    @JsonConstructor
    public CMEntry(final @JsonParameter(readFrom = "execution") Instant executionDate,
                   final @JsonParameter(readFrom = "validation") Instant validationDate,
                   final @JsonParameter(readFrom = "command") String command,
                   final @JsonParameter(readFrom = "plugin") String plugin,
                   final @JsonParameter(readFrom = "successful") boolean wasSuccessful,
                   final @JsonParameter(readFrom = "arguments") String[] arguments) {
        this.executionDate = executionDate;
        this.validationDate = validationDate;
        this.command = command;
        this.plugin = plugin;
        this.wasSuccessful = wasSuccessful;
        this.arguments = arguments;
    }

    /**
     * Get when the command was
     * executed
     *
     * @return the command execution
     * date
     */
    @Override
    public @NotNull Instant getExecutionDate() {
        return this.executionDate;
    }

    /**
     * Get when the command execution
     * was validated. Useful when the
     * execution mode is {@link ProtectionMode#CHAINED}
     * or {@link ProtectionMode#TRUSTED}
     *
     * @return the validation date
     */
    @Override
    public @NotNull Instant getValidatedDate() {
        return this.validationDate;
    }

    /**
     * Get the executed command
     *
     * @return the command that
     * got executed
     */
    @Override
    public @NotNull String getCommand() {
        return this.command;
    }

    /**
     * Get the command plugin. Please
     * note if the command is a bukkit
     * or minecraft, this will return
     * bukkit:server or minecraft:server
     *
     * @return the plugin
     */
    @Override
    public @NotNull String getCommandPlugin() {
        return this.plugin;
    }

    /**
     * Get if the command was
     * successfully executed
     *
     * @return if the command was
     * success
     */
    @Override
    public boolean wasSuccess() {
        return this.wasSuccessful;
    }

    /**
     * Get the executed command
     * arguments. If no arguments
     * were run, this will simply
     * return an empty array
     *
     * @return the arguments of
     * the command
     */
    @Override
    public @NotNull String[] getArguments() {
        return this.arguments;
    }
}

/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.bungee.listener;

import es.karmadev.commandunderme.api.client.command.CommandTicket;
import es.karmadev.commandunderme.api.client.command.exception.ResultCompletedException;
import es.karmadev.commandunderme.api.client.command.history.CommandHistory;
import es.karmadev.commandunderme.api.command.CommandProcessor;
import es.karmadev.commandunderme.api.command.ProtectedCommand;
import es.karmadev.commandunderme.api.plugin.Config;
import es.karmadev.commandunderme.api.plugin.Messages;
import es.karmadev.commandunderme.api.plugin.PluginWrapper;
import es.karmadev.commandunderme.api.util.Utils;
import es.karmadev.commandunderme.plugin.bungee.CMPlugin;
import es.karmadev.commandunderme.plugin.bungee.impl.client.CMClient;
import es.karmadev.commandunderme.plugin.bungee.impl.client.history.CMEntry;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.Connection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;
import org.jetbrains.annotations.NotNull;

import java.time.Instant;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public final class CommandHandler implements Listener {

    private final CMPlugin plugin;

    public CommandHandler(final CMPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onCommand(ChatEvent e) {
        Config config = plugin.getProvider().getConfiguration();
        Connection sender = e.getSender();
        if (!(sender instanceof ProxiedPlayer))
            return;

        ProxiedPlayer player = (ProxiedPlayer) sender;
        CMClient client = plugin.getProvider().getClient(player.getUniqueId())
                .orElseThrow(RuntimeException::new);

        String command = e.getMessage();
        if (client.isThinking()) {
            client.usePassword(command);
            e.setCancelled(true);
            return;
        }

        if (!command.startsWith("/"))
            return;

        command = command.substring(1);
        if (handleHiddenCommand(player, config, plugin.getProvider().getMessages(), command, e))
            return;

        CommandTicket currentTicket = client.getCommandTicket().orElse(null);
        if (isConcurrentCommandAttempt(currentTicket)) {
            logAndCancel(player, e);
            return;
        }

        if (currentTicket != null && currentTicket.isSuccess() && !currentTicket.isConsumed()) {
            currentTicket.consume();

            ProtectedCommand cmd = currentTicket.getCommand();
            if (!cmd.evaluates(command)) {
                client.sendMessage(Utils.colorize("&cUnauthorized", true));
                e.setCancelled(true);
                return;
            }

            return;
        }
        if (currentTicket != null && !currentTicket.isConsumed())
            currentTicket.consume();

        ProtectedCommand protectedCommand = Utils.findProtectedCommand(command);
        if (protectedCommand == null) return;

        if (currentTicket != null && handleExistingTicket(currentTicket, protectedCommand, config, client)) {
            logCommandExecution(true, command, player, client,
                    client.getCommandTicket().orElseThrow(RuntimeException::new), false);
            return;
        }

        initiateNewCommandFlow(player, command, client, protectedCommand);
        e.setCancelled(true); // Prevent execution until password is verified
    }

    private boolean handleHiddenCommand(final ProxiedPlayer player, final Config config,
                                        final Messages messages, final String command,
                                        final ChatEvent event) {
        String cmd = CommandProcessor.getCommandName(command);
        String plugin = CommandProcessor.getCommandPlugin(command);

        Collection<String> hidden = config.getHiddenCommands();
        for (String hiddenCMD : hidden) {
            String hCMD = CommandProcessor.getCommandName(hiddenCMD);
            String hPlugin = CommandProcessor.getCommandPlugin(hiddenCMD);

            if (notEvaluates(cmd, plugin, hCMD, hPlugin))
                continue;

            if (player.hasPermission("cm.execute"))
                return true;

            player.sendMessage(
                    TextComponent.fromLegacy(
                            Utils.colorize(
                                    messages.executionPermission(),
                                    true
                            )
                    )
            );
            event.setCancelled(true);
            return true;
        }

        return false;
    }

    private boolean isConcurrentCommandAttempt(final CommandTicket currentTicket) {
        return currentTicket != null && !currentTicket.isDefined();
    }

    private void logAndCancel(final ProxiedPlayer player, final ChatEvent e) {
        plugin.getLogger().warning(
                String.format("Client %s tried to run more than one command at once", player.getName())
        );
        e.setCancelled(true);
    }

    private boolean handleExistingTicket(final CommandTicket currentTicket, final ProtectedCommand protectedCommand,
                                         final Config config, final CMClient client) {
        if (currentTicket.isSuccess()) {
            switch (config.getProtectionMode()) {
                case CHAINED:
                    if (isWithinChainTime(currentTicket, config)) {
                        updateTicket(currentTicket, protectedCommand, true, client);
                        return true;
                    }
                    break;
                case TRUSTED:
                    updateTicket(currentTicket, protectedCommand, false, client);
                    return true;
                default:
                    break;
            }
        }

        return false;
    }

    private boolean isWithinChainTime(final CommandTicket currentTicket, final Config config) {
        Instant now = Instant.now();
        Instant last = currentTicket.getResultTime();
        return last != null && !now.isAfter(last.plusSeconds(config.getChainTime()));
    }

    private void updateTicket(final CommandTicket currentTicket, final ProtectedCommand protectedCommand, final boolean renew, final CMClient client) {
        CommandTicket updatedTicket = currentTicket.atCommand(protectedCommand, renew);
        client.setTicket(updatedTicket);
    }

    private void initiateNewCommandFlow(final ProxiedPlayer player, final String command, final CMClient client, final ProtectedCommand protectedCommand) {
        CommandTicket newTicket = new CommandTicket(protectedCommand);
        client.setTicket(newTicket);

        client.sendMessage(Utils.colorize(this.plugin.getProvider().getMessages()
                .passwordInChat(), true));
        client.askPassword((result) -> logCommandExecution(result, command, player, client, newTicket, result));
    }

    private void logCommandExecution(final boolean success, final String command, final ProxiedPlayer player,
                                     final CMClient client, final CommandTicket ticket, final boolean execute) {
        String[] arguments = extractCommandArguments(command).toArray(new String[0]);

        try {
            ticket.setResult(success);
        } catch (ResultCompletedException ignored) {}

        ProxyServer.getInstance().getScheduler().schedule(this.plugin, () -> {
            Messages messages = this.plugin.getProvider().getMessages();
            String message;
            if (success) {
                if (execute) {
                    if (player.hasPermission("cm.execute")) {
                        player.sendMessage(TextComponent.fromLegacy(Utils.colorize(
                                messages.runCommandAgain(),
                                true
                        )));
                    } else {
                        player.sendMessage(TextComponent.fromLegacy(Utils.colorize(
                                messages.permission("cm.execute"),
                                true
                        )));
                    }
                }
                message = messages.successBroadcast(client.getName(), ticket.getCommand().getRaw());
            } else {
                if (execute)
                    player.sendMessage(TextComponent.fromLegacy(Utils.colorize(
                            messages.invalidPassword(),
                            true
                    )));

                message = messages.failureBroadcast(client.getName(), ticket.getCommand().getRaw());
            }

            ProxyServer.getInstance().getPlayers().stream().filter((cl) -> !cl.equals(player) && cl.hasPermission("cm.broadcast"))
                    .forEach((cl) -> cl.sendMessage(TextComponent.fromLegacy(Utils.colorize(message, true))));
            ProxyServer.getInstance().getConsole().sendMessage(TextComponent.fromLegacy(Utils.colorize(message, true)));

            this.plugin.requestWebhook(client, success, ticket.getCommand(), arguments);
        }, 0, TimeUnit.SECONDS);

        CommandHistory history = client.getHistory();
        CMEntry entry = new CMEntry(Instant.now(), ticket.getResultTime(),
                ticket.getCommand().getName(),
                ticket.getCommand().getPlugin().map(PluginWrapper::getName).orElse(
                        ticket.getCommand().isBukkit() ? "Bukkit" : "<any>"
                ),
                ticket.isSuccess(),
                arguments);

        history.addEntry(entry);
    }

    private static @NotNull List<String> extractCommandArguments(final String command) {
        if (command.contains(" ")) {
            return Arrays.stream(command.split(" "))
                    .skip(1)
                    .filter(str -> !str.trim().isEmpty())
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    private static boolean notEvaluates(final String source, final String srcPlugin,
                                        final String target, final String trgPlugin) {
        if (trgPlugin != null && srcPlugin == null)
            return true;

        if (srcPlugin != null && trgPlugin != null &&
                !trgPlugin.equalsIgnoreCase(srcPlugin))
            return true;

        return !source.equalsIgnoreCase(target);
    }
}
/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.bungee;

import es.karmadev.commandunderme.api.client.Client;
import es.karmadev.commandunderme.api.command.ProtectedCommand;
import es.karmadev.commandunderme.api.plugin.PluginWrapper;
import es.karmadev.commandunderme.api.util.DiscordWebhook;
import es.karmadev.commandunderme.plugin.bungee.command.CMCommand;
import es.karmadev.commandunderme.plugin.bungee.listener.CommandHandler;
import es.karmadev.commandunderme.plugin.bungee.listener.CompletionHandler;
import es.karmadev.commandunderme.plugin.bungee.listener.ConnectionHandler;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

import java.awt.*;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;

public class CMPlugin extends Plugin {

    private CMProvider provider;

    @Override
    public void onEnable() {
        this.provider = new CMProvider(this);
        this.provider.loadClients();

        setupCommands();
        registerEvents();
    }

    private void setupCommands() {
        Command cmCommand = new CMCommand(this);
        this.getProxy().getPluginManager().registerCommand(this, cmCommand);

        this.provider.detectCommands();
    }

    private void registerEvents() {
        PluginManager manager = this.getProxy().getPluginManager();

        manager.registerListener(this, new ConnectionHandler(this));
        manager.registerListener(this, new CommandHandler(this));
        manager.registerListener(this, new CompletionHandler(this));
    }

    public CMProvider getProvider() {
        return this.provider;
    }

    public void requestWebhook(final Client<ProxiedPlayer> client, final boolean status, final ProtectedCommand command, final String[] arguments) {
        String url = this.provider.getConfiguration().getWebhookURL();
        if (url.trim().isEmpty())
            return;

        DiscordWebhook webhook = new DiscordWebhook(url);
        webhook.setUsername("CommandUnderMe");
        webhook.setAvatarUrl("https://static.wikia.nocookie.net/minecraft_gamepedia/images/c/ca/Milk_Bucket_JE2_BE2.png");

        DiscordWebhook.EmbedObject commandData = new DiscordWebhook.EmbedObject()
                .setTitle("New command execution")
                .setDescription("Minecraft plugin by [KarmaDev](https://github.com/KarmaDeb)")
                .addField("Client", client.getName(), false)
                .addField("Command", command.getName(), true);

        Optional<? extends PluginWrapper<?>> plugin = command.getPlugin();
        if (plugin.isPresent()) {
            commandData.addField("Plugin", plugin.get().getName(), true);
        } else {
            commandData.emptyField();
        }

        commandData.emptyField()
                .addField("Raw command", command.getRaw(), true)
                .addField("Status", (status ? "Success" : "Failed"), true);

        if (arguments.length > 0) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < arguments.length; i++) {
                builder.append(arguments[i]);
                if (i != arguments.length - 1)
                    builder.append(" ");
            }

            commandData.addField("Arguments", builder.toString(), false);
        }

        commandData.setFooter("This message was generated automatically", "");
        if (status) {
            commandData.setColor(Color.GREEN);
        } else {
            commandData.setColor(Color.RED);
        }

        webhook.addEmbed(commandData);
        try {
            webhook.execute();
        } catch (IOException ex) {
            this.getLogger().log(Level.SEVERE, "Tried to log to webhook but an exception occurred", ex);
        }
    }
}

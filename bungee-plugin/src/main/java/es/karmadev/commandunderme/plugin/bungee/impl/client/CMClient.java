/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.bungee.impl.client;

import com.google.common.base.Preconditions;
import es.karmadev.api.kson.io.JsonWriter;
import es.karmadev.api.kson.processor.JsonSerializable;
import es.karmadev.api.kson.processor.construct.JsonConstructor;
import es.karmadev.api.kson.processor.construct.JsonParameter;
import es.karmadev.api.kson.processor.conversor.UUIDTransformer;
import es.karmadev.api.kson.processor.field.JsonElement;
import es.karmadev.api.kson.processor.field.JsonExclude;
import es.karmadev.api.kson.processor.field.Transformer;
import es.karmadev.commandunderme.api.client.Client;
import es.karmadev.commandunderme.api.client.command.CommandTicket;
import es.karmadev.commandunderme.api.client.command.history.CommandHistory;
import es.karmadev.commandunderme.api.util.CryptoHelper;
import es.karmadev.commandunderme.plugin.bungee.CMPlugin;
import es.karmadev.commandunderme.plugin.bungee.impl.client.history.CMHistory;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@JsonSerializable
public final class CMClient implements Client<ProxiedPlayer> {

    @JsonExclude
    private final CMPlugin plugin = (CMPlugin) ProxyServer.getInstance().getPluginManager().getPlugin("CommandUnderMe");

    @JsonExclude
    private ProxiedPlayer player;

    @JsonExclude
    private CompletableFuture<Boolean> passwordTask;

    @JsonElement(name = "username")
    private final String name;

    @JsonElement(name = "uniqueId")
    @Transformer(transformer = UUIDTransformer.class)
    private final UUID uniqueId;

    @JsonElement(name = "history")
    private final CMHistory history;

    @JsonExclude
    private CommandTicket ticket;

    @JsonConstructor
    public CMClient(final @JsonParameter(readFrom = "username") String name,
                    final @JsonParameter(readFrom = "uniqueId") UUID uniqueId,
                    final @JsonParameter(readFrom = "history") CMHistory history) {
        this.player = ProxyServer.getInstance().getPlayer(uniqueId);
        this.name = name;
        this.uniqueId = uniqueId;
        this.history = history;
    }

    /**
     * Load the player into the
     * client
     *
     * @param player the player to load
     */
    public void load(final ProxiedPlayer player) {
        this.player = player;
    }

    /**
     * Get the client name
     *
     * @return the client name
     */
    @Override
    public @NotNull String getName() {
        return this.name;
    }

    /**
     * Get the client unique id
     *
     * @return the unique ID
     */
    @Override
    public @NotNull UUID getUniqueId() {
        return this.uniqueId;
    }

    /**
     * Get the current client command
     * token. The only way a client
     * might not have a ticket is when
     * he joins the game
     *
     * @return the command ticket
     */
    @Override
    public @NotNull Optional<CommandTicket> getCommandTicket() {
        return Optional.ofNullable(this.ticket);
    }

    /**
     * Set the current command
     * ticket
     *
     * @param ticket the client command ticket
     */
    public void setTicket(final CommandTicket ticket) {
        if (this.player == null)
            return;

        this.ticket = ticket;
    }

    public boolean isThinking() {
        return this.passwordTask != null;
    }

    public void askPassword(final Consumer<Boolean> whenComplete) {
        CompletableFuture<Boolean> future = new CompletableFuture<>();
        future.whenComplete((r, e) -> whenComplete.accept(r));

        this.passwordTask = future;
    }

    public void usePassword(final String password) {
        if (this.passwordTask != null) {
            CryptoHelper crypto = this.plugin.getProvider().getCrypto();
            this.passwordTask.complete(crypto.validatePassword(password));

            this.passwordTask = null;
        }
    }

    /**
     * Get the client command
     * history
     *
     * @return the command history
     */
    @Override
    public @NotNull CommandHistory getHistory() {
        return this.history;
    }

    /**
     * Send a message to the client
     *
     * @param message the message to send
     * @param params  the messages parameters
     * @throws NullPointerException if the message is null
     */
    @Override
    public void sendMessage(final String message, final Object... params) throws NullPointerException {
        if (this.isOffline())
            return;

        Preconditions.checkNotNull(message, "Message cannot be null");
        String formatted = this.format(message, params);

        this.player.sendMessage(
                TextComponent.fromLegacy(
                        ChatColor.translateAlternateColorCodes('&', formatted)
                )
        );
    }

    /**
     * Kick the client
     *
     * @param reason the kick reason
     * @param params the message parameters
     * @throws NullPointerException if the kick reason is null
     */
    @Override
    public void kick(final String reason, final Object... params) throws NullPointerException {
        if (this.isOffline())
            return;

        Preconditions.checkNotNull(reason, "Kick reason cannot be null");
        String formatted = this.format(reason, params);

        this.player.disconnect(
                TextComponent.fromLegacy(
                        ChatColor.translateAlternateColorCodes('&', formatted)
                )
        );
    }

    /**
     * Tries to obtain the online
     * player of the client
     *
     * @return the client
     */
    @Override
    public @Nullable ProxiedPlayer getPlayer() {
        return this.player;
    }

    /**
     * Get if the client is currently
     * online
     *
     * @return if the client is online
     */
    @Override
    public boolean isOffline() {
        return this.player == null ||
                !this.player.isConnected();
    }

    /**
     * Save the client
     * @throws IOException if the file fails to save
     */
    @Override
    public void save() throws IOException {
        File clientsFolder = new File(plugin.getDataFolder(), "clients");
        if (!clientsFolder.exists())
            if (!clientsFolder.mkdirs()) {
                plugin.getLogger().warning("Failed to create clients directory");
                return;
            }

        File clientFile = new File(clientsFolder, this.uniqueId.toString().replace("-", "") + ".json");
        if (!clientFile.exists())
            if (!clientFile.createNewFile()) {
                plugin.getLogger().warning("Failed to create client data file");
                return;
            }

        try (FileWriter writer = new FileWriter(clientFile)) {
            JsonWriter wr = new JsonWriter(this);
            wr.export(writer);
            writer.flush();
        }
    }

    /**
     * Get if the client can access
     * other client's history. This method
     * only works if both clients are
     * online
     *
     * @param other the other client
     * @return if the client can access the
     * other client history
     * @throws NullPointerException if the client to check
     *                              with is null
     */
    @Override
    public boolean canAccess(final Client<ProxiedPlayer> other) throws NullPointerException {
        Preconditions.checkNotNull(other, "Client cannot be null");
        if (this.equals(other))
            return true;

        ProxiedPlayer thisPlayer = this.getPlayer();
        ProxiedPlayer otherPlayer = other.getPlayer();

        if (thisPlayer == null || otherPlayer == null)
            return false;

        Set<String> thisPermissions = thisPlayer.getPermissions()
                .stream().filter((str) -> str.matches("^cm\\.privilege\\.[1-9]+[0-9]?$"))
                .collect(Collectors.toSet());
        Set<String> otherPermissions = otherPlayer.getPermissions()
                .stream().filter((str) -> str.matches("^cm\\.privilege\\.[1-9]+[0-9]?$"))
                .collect(Collectors.toSet());

        if (thisPermissions.isEmpty())
            return otherPermissions.isEmpty();

        if (otherPermissions.isEmpty())
            return true;

        int thisLevel = getHighestLevel(thisPermissions);
        int otherLevel = getHighestLevel(otherPermissions);

        return thisLevel > otherLevel;
    }

    private String format(final String text, final Object... parameters) {
        if (parameters == null || parameters.length == 0)
            return text;

        StringBuilder builder = new StringBuilder(text);

        int index = builder.indexOf("{}");
        int paramIndex = 0;
        while (index != -1) {
            Object replacement = parameters[paramIndex++];
            builder.replace(index, index + 2, String.valueOf(replacement));

            if (paramIndex >= parameters.length)
                break;

            index = builder.indexOf("{}");
        }

        return builder.toString();
    }

    private static int getHighestLevel(final Collection<String> permissions) {
        int level = 0;
        for (String str : permissions) {
            int newLevel = getPermissionLevel(str);
            if (newLevel > level)
                level = newLevel;
        }

        return level;
    }

    private static int getPermissionLevel(final String permission) {
        if (permission == null || !permission.contains("."))
            return 0;

        String[] data = permission.split("\\.");
        if (data.length < 3)
            return 0;

        String significantData = data[2];
        try {
            return Integer.parseInt(significantData);
        } catch (NumberFormatException ignored) {}
        return 0;
    }
}

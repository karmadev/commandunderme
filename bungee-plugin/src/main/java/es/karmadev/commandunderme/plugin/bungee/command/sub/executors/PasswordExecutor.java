/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.bungee.command.sub.executors;

import es.karmadev.commandunderme.api.plugin.Messages;
import es.karmadev.commandunderme.api.util.CryptoHelper;
import es.karmadev.commandunderme.api.util.Utils;
import es.karmadev.commandunderme.plugin.bungee.CMPlugin;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;

public final class PasswordExecutor {

    private final CMPlugin plugin;

    public PasswordExecutor(final CMPlugin plugin) {
        this.plugin = plugin;
    }

    public void run(final CommandSender sender, final String password, final String newPassword) {
        final Messages messages = this.plugin.getProvider().getMessages();

        if (!sender.hasPermission("cm.setup")) {
            sender.sendMessage(
                    TextComponent.fromLegacy(
                            Utils.colorize(messages.permission("cm.setup"), true)
                    )
            );
            return;
        }

        CryptoHelper crypto = this.plugin.getProvider().getCrypto();
        if (!crypto.isSetup() || !crypto.updatePassword(password, newPassword)) {
            sender.sendMessage(
                   TextComponent.fromLegacy(
                           Utils.colorize(
                                   messages.passwordFailure(),
                                   true
                           )
                   )
            );
            return;
        }

        sender.sendMessage(
               TextComponent.fromLegacy(
                       Utils.colorize(
                               messages.passwordSuccess(),
                               true
                       )
               )
        );
    }
}

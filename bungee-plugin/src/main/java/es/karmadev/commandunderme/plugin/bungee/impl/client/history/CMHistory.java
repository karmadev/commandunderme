/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.bungee.impl.client.history;

import es.karmadev.api.kson.processor.JsonCollection;
import es.karmadev.api.kson.processor.JsonSerializable;
import es.karmadev.api.kson.processor.construct.CollectionParam;
import es.karmadev.api.kson.processor.construct.JsonConstructor;
import es.karmadev.api.kson.processor.construct.JsonParameter;
import es.karmadev.api.kson.processor.field.JsonElement;
import es.karmadev.commandunderme.api.client.command.history.CommandHistory;
import es.karmadev.commandunderme.api.client.command.history.HistoryEntry;
import es.karmadev.commandunderme.api.client.command.history.filter.HistoryFilter;

import java.util.*;
import java.util.stream.Collectors;

@JsonSerializable
public final class CMHistory implements CommandHistory {

    @JsonElement(name = "entries")
    @JsonCollection
    private final Collection<CMEntry> entries = new LinkedHashSet<>();

    @JsonConstructor
    public CMHistory(final @JsonParameter(readFrom = "entries") @CollectionParam(collectionType = CMEntry.class) Collection<CMEntry> entries) {
        this.entries.addAll(entries);
    }

    /**
     * Get all the history entries
     *
     * @param filters the history filters
     * @return the history entries
     */
    @Override
    public Collection<HistoryEntry> getEntries(final HistoryFilter... filters) {
        if (filters == null || filters.length == 0)
            return Collections.unmodifiableCollection(this.entries);

        Map<Class<? extends HistoryFilter>, Set<HistoryFilter>> filterGroups = new LinkedHashMap<>();
        for (HistoryFilter filter : filters) {
            Class<? extends HistoryFilter> filterClazz = filter.getClass();
            Set<HistoryFilter> value = filterGroups.computeIfAbsent(filterClazz, (e) -> new LinkedHashSet<>());
            value.add(filter);
        }

        return this.entries.stream().filter(e -> {
            for (Set<HistoryFilter> filterGroup : filterGroups.values()) {
                if (filterGroup.stream().noneMatch((f) -> f.evaluates(e)))
                    return false;
            }

            return true;
        }).collect(Collectors.toList());
    }

    /**
     * Add an entry to the
     * history
     *
     * @param entry the entry to add
     */
    @Override
    public void addEntry(HistoryEntry entry) {
        if (!(entry instanceof CMEntry)) {
            entry = new CMEntry(
                    entry.getExecutionDate(),
                    entry.getValidatedDate(),
                    entry.getCommand(),
                    entry.getCommandPlugin(),
                    entry.wasSuccess(),
                    entry.getArguments()
            );
        }

        CMEntry cmEntry = (CMEntry) entry;
        this.entries.add(cmEntry);
    }
}

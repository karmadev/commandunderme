/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.util;

import es.karmadev.commandunderme.api.PluginCM;
import es.karmadev.commandunderme.api.command.ProtectedCommand;
import es.karmadev.commandunderme.api.plugin.Messages;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.TextStyle;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

public final class Utils {

    private final static PluginCM plugin = PluginCM.getInstance();

    private Utils() {}

    public static String colorize(final String str2, final boolean prefix) {
        final Messages messages = plugin.getMessages();
        return String.format("%s%s", (prefix ? messages.getPrefix() : ""), str2)
                .replace("&", "§");
    }

    public static String instantToHuman(final Instant instant) {
        ZonedDateTime zdt = ZonedDateTime.ofInstant(instant, ZoneId.systemDefault());
        return String.format("%s %02d, %d %02d:%02d:%02d",
                zdt.getMonth().getDisplayName(TextStyle.FULL, Locale.getDefault()),
                zdt.getDayOfMonth(),
                zdt.getYear(),
                zdt.getHour(),
                zdt.getMinute(),
                zdt.getSecond());
    }

    public static Instant humanToInstant(final String string, final boolean startOfDay) {
        if (!string.matches("^([aA])?\\d{2}/\\d{2}/\\d{4}$"))
            return null;

        boolean useAmerican = string.toLowerCase().startsWith("a");
        String target = string;
        if (useAmerican)
            target = string.substring(1);

        ZonedDateTime zdt = ZonedDateTime.now();
        int maxDay = zdt.getDayOfMonth();
        int maxMonth = zdt.getMonthValue();
        int maxYear = zdt.getYear();

        String[] data = target.split("/");
        int rawDay = Integer.parseInt(data[0]);
        int rawMonth = Integer.parseInt(data[1]);
        int rawYear = Math.max(maxYear, Integer.parseInt(data[2]));

        if (rawYear == maxYear) {
            if (rawMonth > maxMonth)
                rawMonth = maxMonth;

            if (rawDay > maxDay)
                rawDay = maxDay;
        }

        return zdt.withDayOfMonth(rawDay).withMonth(rawMonth).withYear(rawYear)
                .withHour((startOfDay ? 0 : 23))
                .withMinute((startOfDay ? 0 : 59))
                .withSecond((startOfDay ? 0 : 59))
                .toInstant();
    }

    public static ProtectedCommand findProtectedCommand(final String command) {
        return plugin.getCommands().stream()
                .filter(pc -> pc.evaluates(command))
                .findAny()
                .orElse(null);
    }

    public static Map<String, String[]> extractArguments(final String[] data) {
        Map<String, String[]> arguments = new LinkedHashMap<>();

        String lastParam = null;
        for (String str : data) {
            if (str.startsWith("-")) {
                lastParam = str.substring(1);
                continue;
            }

            if (lastParam != null) {
                arguments.compute(lastParam, (ignored, existingValue) -> {
                    if (existingValue != null) {
                        String[] clone = Arrays.copyOf(existingValue, existingValue.length + 1);
                        clone[clone.length - 1] = str;

                        return clone;
                    }

                    return new String[]{str};
                });
            }
        }

        return arguments;
    }
}

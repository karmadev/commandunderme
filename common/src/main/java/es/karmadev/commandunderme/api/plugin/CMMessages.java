/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.plugin;

import es.karmadev.api.kyle.yaml.YamlContent;
import es.karmadev.commandunderme.api.PluginCM;

import java.nio.file.Files;
import java.nio.file.Path;

public class CMMessages implements Messages {

    private final YamlContent messages;

    public CMMessages(final PluginCM plugin) {
        Path messagesFile = plugin.getDataFolder().resolve("messages.yml");
        if (!Files.exists(messagesFile))
            plugin.extractResource("messages.yml");

        this.messages = YamlContent.load(messagesFile, plugin.getClass().getResourceAsStream("/messages.yml"));
        this.messages.validate();
    }

    /**
     * Reload the configuration
     * file
     */
    @Override
    public void reload() {
        this.messages.reload();
    }

    /**
     * Get the messages prefix
     *
     * @return the prefix
     */
    @Override
    public String getPrefix() {
        return this.messages.getString("Prefix", "&7CommandUnderMe &f: ");
    }

    /**
     * Get the missing permission
     * message
     *
     * @param permission the permission
     * @return the missing permission
     * message
     */
    @Override
    public String permission(final String permission) {
        return this.messages.getString("Permissions", "&cYou do not have the permission: &7{permission}")
                .replace("{permission}", permission);
    }

    /**
     * Get the missing execution
     * permission message
     *
     * @return the missing permission
     * message
     */
    @Override
    public String executionPermission() {
        return this.messages.getString("ExecutionPermission", "&cYou need&7 cm.execute&c permission to run that command");
    }

    /**
     * Get the type password in chat
     * message. Only in bungeecord
     *
     * @return type password in chat
     * message
     */
    @Override
    public String passwordInChat() {
        return this.messages.getString("PasswordInChat", "&cType the password in chat");
    }

    /**
     * Get the invalid password
     * message
     *
     * @return the invalid password
     * message
     */
    @Override
    public String invalidPassword() {
        return this.messages.getString("InvalidPassword","&cInvalid password provided");
    }

    /**
     * Get the successfully typed
     * password message
     *
     * @return the valid password
     * message
     */
    @Override
    public String runCommandAgain() {
        return this.messages.getString("RunCommandAgain", "&aSuccessfully validated command execution, run the command again");
    }

    /**
     * Get the successfully executed
     * command broadcast message
     *
     * @param player  the player who executed
     *                the command
     * @param command the command
     * @return the success broadcast
     * message
     */
    @Override
    public String successBroadcast(final String player, final String command) {
        return this.messages.getString("SuccessBroadcast", "&7{player}&f executed command&6 {command}")
                .replace("{player}", player)
                .replace("{command}", command);
    }

    /**
     * Get the failed executed
     * command broadcast message
     *
     * @param player  the player who executed
     *                the command
     * @param command the command
     * @return the failed broadcast
     * message
     */
    @Override
    public String failureBroadcast(final String player, final String command) {
        return this.messages.getString("FailedBroadcast", "&7{player}&f tried to execute command&6 {command}")
                .replace("{player}", player)
                .replace("{command}", command);
    }

    /**
     * Get the plugin reload
     * success message
     *
     * @return the reload success
     * message
     */
    @Override
    public String reloadSuccess() {
        return this.messages.getString("ReloadSuccess", "&aSuccessfully reloaded plugin files");
    }

    /**
     * Get the plugin reload
     * failure message
     *
     * @return the reload failure
     * message
     */
    @Override
    public String reloadFailure() {
        return this.messages.getString("ReloadFailure", "&cFailed to reload plugin files");
    }

    /**
     * Get the plugin reload
     * usage message
     *
     * @return the reload usage
     * message
     */
    @Override
    public String reloadUsage() {
        return this.messages.getString("ReloadUsage", "&7/cm&f reload&f - &eReloads the plugin files");
    }

    /**
     * Get the plugin setup
     * success message
     *
     * @return the setup success
     * message
     */
    @Override
    public String setupSuccess() {
        return this.messages.getString("SetupSuccess", "&aSuccessfully set the command password");
    }

    /**
     * Get the plugin setup
     * failure message
     *
     * @return the setup failure
     * message
     */
    @Override
    public String setupFailure() {
        return this.messages.getString("SetupFailure", "&cFailed to set the command password, try running &7/cm&f pwd&6 <old password> <new password>");
    }

    /**
     * Get the plugin setup
     * usage message
     *
     * @return the setup usage
     * message
     */
    @Override
    public String setupUsage() {
        return this.messages.getString("SetupUsage", "&7/cm&f setup&6 <password>&f - &eSets the password");
    }

    /**
     * Get the plugin password
     * change success message
     *
     * @return the password change
     * success message
     */
    @Override
    public String passwordSuccess() {
        return this.messages.getString("PasswordSuccess", "&aSuccessfully changed the command password");
    }

    /**
     * Get the plugin password
     * change failure message
     *
     * @return the password change
     * failure message
     */
    @Override
    public String passwordFailure() {
        return this.messages.getString("PasswordFailure", "&cFailed to update the command password");
    }

    /**
     * Get the plugin change
     * password usage message
     *
     * @return the change password
     * usage message
     */
    @Override
    public String passwordUsage() {
        return this.messages.getString("PasswordUsage", "&7/cm&f pwd&7 &6<old> <new>&f - &eUpdates the password");
    }

    /**
     * Get the plugin history player
     * not found message
     *
     * @param player the player
     * @return the player not found message
     */
    @Override
    public String historyNotFound(final String player) {
        return this.messages.getString("HistoryNotFound", "&cCould not find player&7 {player}&c in history")
                .replace("{player}", player);
    }

    /**
     * Get the plugin history player
     * access denied message
     *
     * @param player the player
     * @return the access denied error
     */
    @Override
    public String historyAccessDenied(final String player) {
        return this.messages.getString("HistoryAccessDenied", "&cDenied access to&7 {player}&c history")
                .replace("{player}", player);
    }

    /**
     * Get the plugin command history
     * usage message
     *
     * @return the command history
     * usage message
     */
    @Override
    public String historyUsage() {
        return this.messages.getString("HistoryUsage", "&7/cm&f history &6<player>&f - &eShows command history");
    }
}
/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.util;

import es.karmadev.commandunderme.api.PluginCM;

import java.io.*;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class CryptoHelper {

    private final File pwdFile;
    private String storedPassword;

    public CryptoHelper(final PluginCM plugin) {
        this.pwdFile = plugin.getDataFolder().resolve(".pwd").toFile();
        this.storedPassword = this.loadPassword();
    }

    public boolean isSetup() {
        return this.pwdFile.exists() && !this.isFileEmpty();
    }

    public boolean setup(final String password) {
        if (password.trim().isEmpty()) {
            return false;
        }

        try {
            if (this.pwdFile.exists() && !this.isFileEmpty()) {
                return false;
            }

            this.storedPassword = this.storePassword(password);
            return this.storedPassword != null;
        } catch (IOException ex) {
            return false;
        }
    }

    public boolean updatePassword(final String oldPassword, final String newPassword) {
        if (oldPassword.trim().isEmpty() || newPassword.trim().isEmpty()) {
            return false;
        }

        try {
            if (!this.pwdFile.exists() || !this.validatePassword(oldPassword)) {
                return false;
            }

            this.storedPassword = this.storePassword(newPassword);
            return this.storedPassword != null;
        } catch (IOException ex) {
            return false;
        }
    }

    public boolean validatePassword(final String password) {
        if (this.storedPassword == null)
            this.storedPassword = this.loadPassword();

        if (this.storedPassword == null)
            return false;

        if (password.trim().isEmpty()) {
            return false;
        }

        byte[] digest = this.makeDigest(password);
        if (digest == null) {
            return false;
        }

        String hexDigest = this.bytesToHex(digest);
        return hexDigest.equals(this.storedPassword);
    }

    private String loadPassword() {
        if (!this.pwdFile.exists() || this.isFileEmpty())
            return null;

        try {
            byte[] data = Files.readAllBytes(this.pwdFile.toPath());
            return this.bytesToHex(data);
        } catch (IOException ex) {
            return null;
        }
    }

    private byte[] makeDigest(final String content) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(content.getBytes());
            return messageDigest.digest();
        } catch (NoSuchAlgorithmException ex) {
            return null;
        }
    }

    private String bytesToHex(final byte[] bytes) {
        StringBuilder hexString = new StringBuilder();
        for (byte b : bytes) {
            String hex = Integer.toHexString(0xff & b);
            if (hex.length() == 1)
                hexString.append('0');

            hexString.append(hex);
        }
        return hexString.toString();
    }

    private boolean isFileEmpty() {
        return this.pwdFile.length() == 0;
    }

    private String storePassword(final String password) throws IOException {
        byte[] digest = this.makeDigest(password);
        if (digest == null) {
            return null;
        }

        Files.write(this.pwdFile.toPath(), digest);
        return this.bytesToHex(digest);
    }
}
/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.api.plugin;

import es.karmadev.api.kyle.yaml.YamlContent;
import es.karmadev.commandunderme.api.PluginCM;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;

public class CMConfig implements Config {

    private final YamlContent config;

    public CMConfig(final PluginCM plugin) {
        Path configFile = plugin.getDataFolder().resolve("config.yml");
        if (!Files.exists(configFile))
            plugin.extractResource("config.yml");

        this.config = YamlContent.load(configFile, plugin.getClass().getResourceAsStream("/config.yml"));
        this.config.validate();
    }

    /**
     * Reload the configuration
     * file
     */
    @Override
    public void reload() {
        this.config.reload();
    }

    /**
     * Get a list containing all
     * the blocked commands
     *
     * @return the blocked
     * commands
     */
    @Override
    public Collection<String> getBlockedCommands() {
        List<String> blocked = this.config.getList("ProtectedCommands");
        blocked.removeAll(this.getHiddenCommands());

        return blocked;
    }

    /**
     * Get a list containing all
     * the hidden commands
     *
     * @return the hidden
     * commands
     */
    @Override
    public Collection<String> getHiddenCommands() {
        return this.config.getList("HiddenCommands");
    }

    /**
     * Get the command protection
     * mode
     *
     * @return the protection mode
     */
    @Override
    public ProtectionMode getProtectionMode() {
        String rawMode = this.config.getString("Mode", "CHAINED")
                .toUpperCase();

        try {
            return ProtectionMode.valueOf(rawMode);
        } catch (IllegalArgumentException ex) {
            this.config.set("Mode", "CHAINED");
            this.config.save();

            return ProtectionMode.CHAINED;
        }
    }

    /**
     * Get the amount of seconds a
     * client is allowed to run commands
     * without authenticating when
     * in chain mode
     *
     * @return the chain time
     */
    @Override
    public int getChainTime() {
        int time = this.config.getInteger("ChainTime", 5);
        return Math.max(5, Math.min(30, time));
    }

    public String getWebhookURL() {
        return this.config.getString("DiscordWebhook", "");
    }
}

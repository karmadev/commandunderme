# CommandUnderMe
CommandUnderMe is an open-source plugin developed by **KarmaDev** which protects commands
execution under a password rather than simply blocking them and allowing
staff members to run them with a permissions.

## IMPORTANT: Support
Support is strictly granted to the ones who paid for the compiled version
of the plugin or those who contributed to this project, by creating a pull
request with a new feature or bug fix, or by becoming a "bug hunter". If you
are a contributor (paid for the plugin, or meet the requirements specified
above, you can join the [discord](https://discord.reddo.es) to obtain support)

### Why CommandUnderMe
Even though there are a lot of plugins which do something similar to CommandUnderMe,
this plugin protects even more your server by protecting those commands behind
a password, which only your staff member should know. Of course, they still need
a permission to run those commands, but if someone manages to break
into an admin account because his password was weak, or because his account got
stolen, that staff member will still need the password to access the command.

### Setup
Setting up the plugin is really easy, it doesn't have any dependencies, so you
can just install it and start configuring it. First of all, you have two ways of
run this plugin.

<details>
    <summary>With password (RECOMMENDED!)</summary>
The most recommended and secure way. If you run the plugin with a defined password,
the commands with not be only behind a required permission (cm.execute), but also behind
a password, in case any of your staff member accounts gets hacked.
</details>

<details>
    <summary>Without password</summary>
You can run the plugin without a password defined, this will make the plugin simply
avoid players without the permission cm.execute to run protected commands.
</details>

If you decide to run the plugin with a password, all you have to do is run the setup command
from console. In case you are running on a PaperMC server, you will have to either run the
command from game with a client with `cm.setup` permission, or grant the `cm.setup` permission
to the console.

```
cm setup <password> 
```

If you typed the password wrongly, or just want to change it, you can use the `pwd` command
to change the password

```
cm pwd <old password> <new password>
```

#### Removing the password
If you want to remove the current password, because you forgot it, or because you want
to simply stop using it, you can simply remove the `.pwd` file in the plugin folder, and restart
your server.

### Discord
Another difference against other plugins is that, CommandUnderMe logs command executions, with
useful information such as additional information about the command that was run, the date when it
was run etc. And this, can also be logged into discord, by using a websocket, and defining it in
the config.yml, the plugin will log any protected command execution into the discord server, with
information such as the client who run the command, and all the previously mentioned
information.

### Viewing commands history
As the plugin logs the protected commands a client tried to run, you can view all those by running
the command `cm history <player>`. This command is a bit advanced, because it has some arguments which
allows you to filter the history results.

If you run this command from the game, it will open an inventory with all the run commands, but if
you run this command from a console, it will be displayed as text with a pagination system (`cm history
<player> <page number>`).

#### History arguments
Those are all the arguments you can apply to the history command to filter the results:
> -from [date]

The from argument allows you to define a minimum date that the command must have run
since. For instance, if you run `cm history KarmaDev -from 01/01/1999` it will show all
the commands the player KarmaDev have run since January 01st, 1999. By default, the date
notation uses Gregorian calendar (day, month and year) `cm history KarmaDev -from 26/05/2024`, but you can use American one by
simply adding `a` before the date. (`cm history KarmaDev -from a05/26/2024`).

> -to [date]

Similar to the `from` argument, this argument allows you to define a maximum date that the command
must have run before. For instance; `cm history KarmaDev -to 06/08/2025` will show all commands that
have been run since `01/01/1999` till `06/08/2025`. You can combine this with `from` to create a date
range `cm history KarmaDev -from 01/01/2023 -to 31/12/2024`.

> -command [command]
 
This argument allows you to filter by command run. For instance, let's say that you have protected the
commands `bukkit:help`, `plugin:heal` and `fly`, and you want to obtain only the command history on KarmaDev
for the command heal. You can run `cm history KarmaDev -command plugin:heal` (or `cm history KarmaDev -command heal`)
to obtain the executions of the heal command of KarmaDev. You can add as many commands as you want. For instance
`cm history KarmaDev -command plugin:heal -command fly` would show all the command history for `plugin:heal` and
`fly` of KarmaDev

> -result [y|success|true|yes|executed]
 
This argument allows you to filter the history by execution result. There are only two execution results: Success and Failed.
If you provide a value of -result other rather than `y, success, true, yes or executed`, it will display only failed executions.

A command is considered to be `Success` when the password was correct, in case there is no password defined, success will be always
true

#### Example:
`cm history KarmaDev -from 01/01/2024 -to 31/12/2024 -command op -result y`
Would display all the command `op` successful executions during `2024` which result was successfully of player `KarmaDev`

### Credits

Those libraries are used internally, and are essential for the plugin to work:
- [AnvilGUI](https://github.com/WesJD/AnvilGUI) by [WestJD](https://wesleysmith.dev/) (MIT)
- [FunctionalInventory](https://gitlab.com/karmadev/functionalinventory) by [KarmaDev](https://gitlab.com/karmadev/) (MIT)
- [KSon](https://gitlab.com/karmadev/kson) by [KarmaDev](https://gitlab.com/karmadev/) (GNU GPLv3)

### License

```
This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
```

### MIT Libraries

This project uses MIT libraries, those being:

**AnvilGUI**
```
MIT License

Copyright (c) 2016 Wesley Smith

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
> This project does not set under GPL v3 to this AnvilGUI
/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.spigot.command.sub.executors;

import es.karmadev.api.spigot.helper.Colorize;
import es.karmadev.api.spigot.inventory.helper.exceptions.EmptyBookException;
import es.karmadev.api.spigot.inventory.helper.exceptions.NoIndexPageException;
import es.karmadev.api.spigot.inventory.helper.infinity.InventoryBook;
import es.karmadev.api.spigot.inventory.helper.infinity.InventoryPage;
import es.karmadev.api.spigot.reflection.skull.SkullBuilder;
import es.karmadev.commandunderme.api.client.command.history.CommandHistory;
import es.karmadev.commandunderme.api.client.command.history.HistoryEntry;
import es.karmadev.commandunderme.api.client.command.history.filter.CommandFilter;
import es.karmadev.commandunderme.api.client.command.history.filter.HistoryFilter;
import es.karmadev.commandunderme.api.client.command.history.filter.ResultFilter;
import es.karmadev.commandunderme.api.client.command.history.filter.TimestampFilter;
import es.karmadev.commandunderme.api.command.CommandProcessor;
import es.karmadev.commandunderme.api.plugin.Messages;
import es.karmadev.commandunderme.plugin.spigot.CMPlugin;
import es.karmadev.commandunderme.plugin.spigot.impl.client.CMClient;
import es.karmadev.commandunderme.api.util.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;

import java.time.Instant;
import java.util.*;
import java.util.logging.Level;

public class HistoryExecutor {

    private final static String ENTRY_VALUE = "ewogICJ0aW1lc3RhbXAiIDogMTY0MTQ4NzkwNTk3NCwKICAicHJvZmlsZUlkIiA6ICIyMWUzNjdkNzI1Y2Y0ZTNiYjI2OTJjNGEzMDBhNGRlYiIsCiAgInByb2ZpbGVOYW1lIiA6ICJHZXlzZXJNQyIsCiAgInNpZ25hdHVyZVJlcXVpcmVkIiA6IHRydWUsCiAgInRleHR1cmVzIiA6IHsKICAgICJTS0lOIiA6IHsKICAgICAgInVybCIgOiAiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS9kNzc4MzI4MmUxNzEzNjJjZDM2ZTQzZGZlN2IxYjBkNmM2ZTlkYTQyY2I1NTMzZDI5MmE5NTc5YTY0NDU3MDk1IgogICAgfQogIH0KfQ==";
    private final static String ENTRY_SIGNATURE = "awFHk57eZIRvXDmUeZg+RPvxnufrdqMiNkKR95SdBmRUrSvTAAYDzcFFNr25/9ss7JViyZp0yzDPRfE3QZIjn2ZazgGy2U4nlKJ/G8ZXssFn/TV4G1DWu9FkjiGDa6V6mexMcMMm+nM8n0aZkpkRoO6myzcA2VR6HgpeSE2CZ4yut5B2bzxsEXeF3NCHi2tjm6gc//oExa/t+0J2NAopPDw8QDDIYz6qeW+bNOroGijYx7hVOlnFL/eYMo+8H0oH0bCYWgY9yCp0vweAf/YPxcNcFamJJMeNn6gisBi8e2AsNnTfxYSVdH5JitOeqceEKXfoZoJCeAWtKgD1f0HymBVg4KVQ+GZqUpcfc0D/nl/Xta1C9uzKzZLIZAwfSZ9zDPFujb4OEM9xtEac3Q0ykDafwh37uWcdKSLbr9L/q7wMqxpKE0nI+oPe982gAgvdJEipHZy+2ZPdYw2jVNkmd4x5TM2wnyVbSmVG7NMS0g5QJrnS8K93Hgmo3tOpchNOLWrClmTwFfsyGmPdJGs7QHbyyYwgucTCRXSis6BLtMZHvcF9oae1Td8uyPEONvi6L/XVVIqjSP2PIeonNxLVrtdZnk8VDnIPzFGoDdO5FcHh6naqGb9PFIX1zCdVTL5OM3ke5If7AH5xT3MggbVw8VK2pyfxaGI0ngvMB7/5K8A=";

    private final CMPlugin plugin;

    public HistoryExecutor(final CMPlugin plugin) {
        this.plugin = plugin;
    }

    public void run(final CommandSender sender, final String[] args) {
        Messages messages = this.plugin.getProvider().getMessages();
        if (!sender.hasPermission("cm.history")) {
            sender.sendMessage(Utils.colorize(messages.permission("cm.history"), true));
            return;
        }

        String target = args[0];
        UUID uniqueId = parseID(target);
        CMClient client = this.plugin.getProvider().getClient(uniqueId).orElse(null);
        if (client == null) {
            Player player = this.plugin.getServer().getPlayer(target);
            if (player != null && player.isOnline()) {
                client = this.plugin.getProvider().getClient(player.getUniqueId()).orElse(null);
            }
        }

        if (client == null) {
            sender.sendMessage(
                    Utils.colorize(messages.historyNotFound(target), true)
            );
            return;
        }

        boolean hasAccess = true;
        if (sender instanceof Player) {
            Player executor = (Player) sender;
            CMClient executorClient = this.plugin.getProvider().getClient(executor.getUniqueId())
                    .orElseThrow(RuntimeException::new);

            hasAccess = executorClient.canAccess(client);
        }

        if (!hasAccess) {
            sender.sendMessage(
                    Utils.colorize(messages.historyAccessDenied(target),
                            true)
            );
            return;
        }

        List<HistoryFilter> filters = parseFilters(sender, args);
        if (filters == null)
            return;

        CommandHistory history = client.getHistory();
        if (sender instanceof Player) {
            printHistoryToPlayer((Player) sender, client, history, filters.toArray(new HistoryFilter[0]));
        } else {
            printHistoryToSender(sender, args, history, filters.toArray(new HistoryFilter[0]));
        }
    }

    private List<HistoryFilter> parseFilters(final CommandSender sender, final String[] args) {
        Map<String, String[]> rawArguments = Utils.extractArguments(args);
        List<HistoryFilter> filters = new ArrayList<>();

        TimestampFilter.FilterBuilder builder = TimestampFilter.builder();
        if (readFromArgument(rawArguments, "from", true, sender, builder)) return null;
        if (readFromArgument(rawArguments, "to", false, sender, builder)) return null;
        readCommandArgument(rawArguments, filters);
        readResultArgument(rawArguments, filters);

        filters.add(builder.build());
        return filters;
    }

    private static void readResultArgument(Map<String, String[]> rawArguments, List<HistoryFilter> filters) {
        if (rawArguments.containsKey("result")) {
            String[] arguments = rawArguments.get("result");
            if (arguments != null && arguments.length > 0) {
                String result = arguments[0].toLowerCase();
                filters.add(result.equals("y") || result.equals("success") ||
                        result.equals("true") || result.equals("yes") ||
                        result.equals("executed") ? ResultFilter.SUCCESS : ResultFilter.FAILED);
            }
        }
    }

    private static void readCommandArgument(Map<String, String[]> rawArguments, List<HistoryFilter> filters) {
        if (rawArguments.containsKey("command")) {
            String[] arguments = rawArguments.get("command");
            for (String str : arguments) {
                try {
                    String cName = CommandProcessor.getCommandName(str);
                    String cPlugin = CommandProcessor.getCommandPlugin(str);

                    CommandFilter filter = CommandFilter.builder()
                            .forCommand(cName)
                            .forPlugin(cPlugin)
                            .build();
                    filters.add(filter);
                } catch (Throwable ignored) {}
            }
        }
    }

    private static boolean readFromArgument(Map<String, String[]> rawArguments, String from, boolean startOfDay, CommandSender sender,
                                            TimestampFilter.FilterBuilder builder) {
        if (rawArguments.containsKey(from)) {
            String[] content = rawArguments.get(from);
            if (content != null && content.length > 0) {
                String firstEntry = content[0];
                Instant instant = Utils.humanToInstant(firstEntry, startOfDay);

                if (instant == null) {
                    sender.sendMessage(
                            Utils.colorize("&cInvalid time format, expected day/month/year or [a]month/day/year", true)
                    );
                    return true;
                }

                if (from.equals("from")) {
                    builder.from(instant);
                } else {
                    builder.to(instant);
                }
            }
        }
        return false;
    }

    private void printHistoryToSender(CommandSender sender, String[] args, CommandHistory history, final HistoryFilter[] filters) {
        int page = 1;
        if (args.length >= 2) {
            try {
                page = Math.max(1, Integer.parseInt(args[1]));
            } catch (NumberFormatException ignored) {}
        }

        HistoryEntry[] entryArray = history.getEntries(filters).toArray(new HistoryEntry[0]);
        int maxPage = entryArray.length / 5;
        if (maxPage < (page - 1)) {
            sender.sendMessage(
                    Utils.colorize("&cCannot read page&7 " + page + "&c. Maximum is:&7 " + maxPage, true)
            );
            return;
        }

        int indexStart = (page - 1) * 5;
        HistoryEntry[] entries = splitEntries(entryArray, indexStart, 5);

        for (HistoryEntry entry : entries) {
            printEntry(entry, sender);
        }
        sender.sendMessage(Colorize.colorize("&7Page: &e" + page + "&8 | &6" + (maxPage + 1)));
    }

    private void printHistoryToPlayer(Player sender, CMClient client, CommandHistory history, final HistoryFilter[] filters) {
        InventoryBook book = new InventoryBook(plugin, "&3" + client.getName());
        setupPage(book, sender, client.getName(), history, filters, 1, 0);

        try {
            book.open(sender);
        } catch (EmptyBookException | NoIndexPageException ex) {
            this.plugin.getLogger().log(Level.SEVERE, "Failed to provide history of " + client.getName(), ex);
        }
    }

    private void printEntry(final HistoryEntry entry, final CommandSender sender) {
        List<String> messages = buildEntryLore(entry);
        messages.forEach(sender::sendMessage);
    }

    private UUID parseID(final String target) {
        UUID full = fromTrimmed(target);
        if (full != null) return full;

        try {
            return UUID.fromString(target);
        } catch (IllegalArgumentException ignored) {}

        return UUID.nameUUIDFromBytes(("OfflinePlayer:" + target).getBytes());
    }

    private UUID fromTrimmed(final String str) {
        if (str.length() != 32)
            return null;

        String uuidWithHyphens = str.replaceFirst(
                "(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})",
                "$1-$2-$3-$4-$5"
        );
        return UUID.fromString(uuidWithHyphens);
    }

    private void setupPage(final InventoryBook book, final Player player, final String target, final CommandHistory history,
                           final HistoryFilter[] filters, final int pageNum, final int index) {
        HistoryEntry[] entryArray = history.getEntries(filters).toArray(new HistoryEntry[0]);
        HistoryEntry[] firstPageEntries = splitEntries(entryArray, index, 45);

        int currentPage = book.getPageIndex(player);
        int maxPage = book.getPages();

        if ((currentPage + 1) == maxPage) {
            if (book.getPage(currentPage + 1) != null) return;
            InventoryPage page = book.addPage(String.format("&3%s page %d", target, pageNum));

            for (HistoryEntry entry : firstPageEntries) {
                ItemStack item = makeEntryStack(entry);
                page.addItem(item);
            }

            page.beforeNext(() -> {
                Collection<HistoryEntry> entries = history.getEntries();
                if (entries.size() <= index)
                    return;

                setupPage(book, player, target, history, filters, pageNum + 1, index + 45);
            });
        }
    }

    private ItemStack makeEntryStack(final HistoryEntry entry) {
        ItemStack item = SkullBuilder.createSkull(ENTRY_VALUE, ENTRY_SIGNATURE);
        ItemMeta meta = item.getItemMeta();
        assert meta != null;

        meta.setDisplayName(String.format(
                Colorize.colorize("&6/&e%s"),
                entry.getCommand()
        ));
        List<String> lore = buildEntryLore(entry);
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.values());

        item.setItemMeta(meta);
        return item;
    }

    private static @NotNull List<String> buildEntryLore(final HistoryEntry entry) {
        List<String> lore = new ArrayList<>();
        lore.add(Colorize.colorize("&3&m---------------"));
        lore.add("");
        lore.add(Colorize.colorize("&7Plugin&8:&b " + entry.getCommandPlugin()));
        lore.add(Colorize.colorize("&7Status&8:&b " + (entry.wasSuccess() ? "Success" : "Failed")));
        lore.add(Colorize.colorize("&7Executed&8:&b " + Utils.instantToHuman(entry.getExecutionDate())));
        lore.add(Colorize.colorize("&7Validation&8:&b " + Utils.instantToHuman(entry.getValidatedDate())));
        String[] arguments = entry.getArguments();
        if (arguments.length > 0) {
            lore.add(Colorize.colorize("&7Arguments&8:"));
            for (String argument : arguments) {
                lore.add(Colorize.colorize(" &7-&b " + argument));
            }
        }

        return lore;
    }

    private HistoryEntry[] splitEntries(final HistoryEntry[] array, final int from, final int items) {
        if (array.length == 0 || from >= array.length) {
            return new HistoryEntry[0];
        }

        int length = Math.min(items, array.length - from);
        HistoryEntry[] result = new HistoryEntry[length];
        System.arraycopy(array, from, result, 0, length);

        return result;
    }
}

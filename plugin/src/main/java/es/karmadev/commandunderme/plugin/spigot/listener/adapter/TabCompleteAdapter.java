/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.spigot.listener.adapter;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.utility.MinecraftVersion;
import es.karmadev.commandunderme.api.client.command.CommandTicket;
import es.karmadev.commandunderme.api.command.CommandProcessor;
import es.karmadev.commandunderme.api.command.ProtectedCommand;
import es.karmadev.commandunderme.api.plugin.Config;
import es.karmadev.commandunderme.api.plugin.ProtectionMode;
import es.karmadev.commandunderme.plugin.spigot.CMPlugin;
import es.karmadev.commandunderme.plugin.spigot.impl.client.CMClient;
import es.karmadev.commandunderme.api.util.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;

import java.time.Instant;
import java.util.Collection;

public class TabCompleteAdapter extends PacketAdapter {

    private final CMPlugin plugin;

    public TabCompleteAdapter(final CMPlugin plugin) {
        super(plugin, PacketType.Play.Client.TAB_COMPLETE);
        this.plugin = plugin;
    }

    @Override
    public void onPacketReceiving(final PacketEvent event) {
        PacketType packetType = event.getPacketType();
        if (packetType.equals(PacketType.Play.Client.TAB_COMPLETE)) {
            Player player = event.getPlayer();
            CMClient client = plugin.getProvider().getClient(player.getUniqueId())
                    .orElseThrow(RuntimeException::new);

            PacketContainer container = event.getPacket();
            String command = container.getSpecificModifier(String.class)
                    .read(0).toLowerCase();
            Config configuration = this.plugin.getProvider().getConfiguration();

            if (!player.hasPermission("cm.execute")) {
                Collection<String> hidden = configuration.getHiddenCommands();

                String cmName = CommandProcessor.getCommandName(command);
                String cmPlugin = CommandProcessor.getCommandPlugin(command);
                for (String str : hidden) {
                    String sName = CommandProcessor.getCommandName(str);
                    String sPlugin = CommandProcessor.getCommandPlugin(str);

                    if (evaluates(cmName, cmPlugin, sName, sPlugin)) {
                        event.setCancelled(true);
                        return;
                    }
                }

                if (event.isCancelled())
                    return;
            }

            ProtectedCommand protectedCommand = Utils.findProtectedCommand(command);
            if (protectedCommand == null) return;

            if (configuration.getProtectionMode().equals(ProtectionMode.STRICT)) {
                event.setCancelled(true);
                return;
            }

            CommandTicket ticket = client.getCommandTicket().orElse(null);
            switch (configuration.getProtectionMode()) {
                case TRUSTED:
                    if (ticket == null || !ticket.isDefined() || !ticket.isSuccess()) {
                        event.setCancelled(true);
                    }

                    break;
                case CHAINED:
                default:
                    if (ticket == null || !ticket.isDefined() || isOutChainTime(ticket, configuration)) {
                        event.setCancelled(true);
                    }
            }
        }
    }

    private boolean isOutChainTime(final CommandTicket currentTicket, final Config config) {
        Instant now = Instant.now();
        Instant last = currentTicket.getResultTime();
        return last == null || now.isAfter(last.plusSeconds(config.getChainTime()));
    }

    public static void register(final CMPlugin plugin) {
        PluginManager manager = Bukkit.getServer().getPluginManager();
        if (!manager.isPluginEnabled("ProtocolLib")) {
            plugin.getLogger().warning("ProtocolLib has not been detected. Command completions won't be hidden (You can ignore this if you are over 1.12)");
            return;
        }

        if (MinecraftVersion.getCurrentVersion().isAtLeast(new MinecraftVersion(1, 12, 2))) {
            ProtocolLibrary.getProtocolManager().addPacketListener(
                    new TabCompleteAdapter(plugin)
            );
        }
    }

    private boolean evaluates(final String source, final String srcPlugin,
                              final String target, final String trgPlugin) {
        if (trgPlugin != null && srcPlugin == null)
            return false;

        if (srcPlugin != null && trgPlugin != null &&
                !trgPlugin.equalsIgnoreCase(srcPlugin))
            return false;

        return source.equalsIgnoreCase(target);
    }
}

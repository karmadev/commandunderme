/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.spigot;

import es.karmadev.api.kson.KsonException;
import es.karmadev.api.kson.io.JsonReader;
import es.karmadev.commandunderme.api.PluginCM;
import es.karmadev.commandunderme.api.command.CommandProcessor;
import es.karmadev.commandunderme.api.plugin.CMConfig;
import es.karmadev.commandunderme.api.plugin.CMMessages;
import es.karmadev.commandunderme.plugin.spigot.impl.client.CMClient;
import es.karmadev.commandunderme.plugin.spigot.impl.client.history.CMHistory;
import es.karmadev.commandunderme.plugin.spigot.impl.command.CMProtected;
import es.karmadev.commandunderme.api.util.CryptoHelper;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.stream.Collectors;

public class CMProvider extends PluginCM {

    private final CMPlugin plugin;

    private final CryptoHelper crypto;
    private final Collection<CMProtected> commands = ConcurrentHashMap.newKeySet();
    private final Collection<CMClient> clients = ConcurrentHashMap.newKeySet();

    private final CMConfig config;
    private final CMMessages messages;

    CMProvider(final CMPlugin plugin) {
        this.plugin = plugin;
        this.register();

        this.crypto = new CryptoHelper(this);
        this.config = new CMConfig(this);
        this.messages = new CMMessages(this);
    }

    /**
     * Get the protected commands
     *
     * @return the protected commands
     */
    @Override
    public Collection<CMProtected> getCommands() {
        return Collections.unmodifiableCollection(this.commands);
    }

    /**
     * Get all the stored clients
     *
     * @return the stored
     * clients
     */
    @Override
    public Collection<CMClient> getClients() {
        return Collections.unmodifiableCollection(this.clients);
    }

    /**
     * Get a client
     *
     * @param uniqueId the client unique id
     * @return the client
     */
    @Override
    public Optional<CMClient> getClient(final UUID uniqueId) {
        return this.clients.stream()
                .filter(client -> clientMatchesUUID(client, uniqueId))
                .findAny();
    }

    /**
     * Get the plugin configuration
     *
     * @return the configuration
     */
    @Override
    public CMConfig getConfiguration() {
        return this.config;
    }

    /**
     * Get the plugin messages
     *
     * @return the messages
     */
    @Override
    public CMMessages getMessages() {
        return this.messages;
    }

    /**
     * Get the plugin data folder
     *
     * @return the plugin data folder
     */
    @Override
    public Path getDataFolder() {
        return this.plugin.getDataFolder().toPath();
    }

    /**
     * Extract a resource
     *
     * @param resourceName the resource to
     *                     extract
     * @return if the resource was extracted
     */
    @Override
    public boolean extractResource(final String resourceName) {
        try {
            this.plugin.saveResource(resourceName, true);
            return true;
        } catch (IllegalArgumentException ex) {
            return false;
        }
    }

    public CryptoHelper getCrypto() {
        return this.crypto;
    }

    public void detectCommands() {
        List<String> containedCommands = getContainedCommands();
        List<String> protectedCommands = getProtectedCommands();

        removeInvalidCommands(protectedCommands);
        protectedCommands.removeAll(containedCommands);

        addNewProtectedCommands(protectedCommands);
    }

    public void addClient(final CMClient client) {
        this.clients.add(client);
    }

    private List<String> getContainedCommands() {
        return this.commands.stream()
                .map(cmd -> cmd.isBukkit() ? String.format("bukkit:%s", cmd.getName()) : cmd.getRaw())
                .collect(Collectors.toList());
    }

    private List<String> getProtectedCommands() {
        return this.config.getBlockedCommands()
                .stream()
                .map(String::toLowerCase)
                .collect(Collectors.toList());
    }

    private void removeInvalidCommands(final List<String> protectedCommands) {
        this.commands.removeIf(cmd -> !protectedCommands.contains(cmd.getRaw()));
    }

    private void addNewProtectedCommands(final List<String> protectedCommands) {
        if (!protectedCommands.isEmpty()) {
            for (String str : protectedCommands) {
                String cmdName = CommandProcessor.getCommandName(str);
                String pluginName = CommandProcessor.getCommandPlugin(str);
                this.commands.add(new CMProtected(pluginName, cmdName));
            }
        }
    }

    void loadClients() {
        Set<Player> ignore = new HashSet<>();

        File clientsDir = new File(this.plugin.getDataFolder(), "clients");
        if (clientsDir.exists()) {
            File[] files = clientsDir.listFiles();

            if (files != null) {
                for (File file : files) {
                    try (FileReader reader = new FileReader(file)) {
                        CMClient client = JsonReader.load(CMClient.class, reader);
                        this.clients.add(client);

                        Player online = this.findPlayer(client, ignore);
                        if (online == null) continue;

                        ignore.add(online);
                        client.load(online);
                    } catch (KsonException | IOException ex) {
                        this.plugin.getLogger().log(Level.SEVERE, "Failed to load client file " + file.getName(), ex);
                    }
                }
            }
        }

        Bukkit.getOnlinePlayers().stream().filter((p) -> !ignore.contains(p))
                .forEach((player) -> {
                    String name = player.getName();
                    UUID uuid = UUID.nameUUIDFromBytes(("OfflinePlayer:" + name).getBytes());

                    CMClient client = new CMClient(player.getName(), uuid, new CMHistory(
                            Collections.emptyList()
                    ));
                    client.load(player);

                    this.clients.add(client);
                    try {
                        client.save();
                    } catch (IOException ex) {
                        this.plugin.getLogger().log(Level.SEVERE, "Failed to store player " + player.getName(), ex);
                    }
                });
    }

    private Player findPlayer(final CMClient client, final Set<Player> ignore) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (ignore.contains(player)) continue;

            String name = player.getName();

            UUID offlineId = UUID.nameUUIDFromBytes(("OfflinePlayer:" + name).getBytes());
            UUID playerId = player.getUniqueId();
            if (client.getUniqueId().equals(playerId) ||
                    client.getUniqueId().equals(offlineId))
                return player;

            if (client.getName().equals(name))
                return player;
        }

        return null;
    }

    private boolean clientMatchesUUID(final CMClient client, final UUID uniqueId) {
        Player online = client.getPlayer();
        if (client.getUniqueId().equals(uniqueId))
            return true;

        if (online == null) return false;
        return online.getUniqueId().equals(uniqueId);
    }
}

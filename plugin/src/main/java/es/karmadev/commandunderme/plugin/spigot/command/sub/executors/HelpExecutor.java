/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.spigot.command.sub.executors;

import es.karmadev.commandunderme.api.plugin.Messages;
import es.karmadev.commandunderme.plugin.spigot.CMPlugin;
import es.karmadev.commandunderme.api.util.Utils;
import org.bukkit.command.CommandSender;

public final class HelpExecutor {

    private final CMPlugin plugin;
    private String helpMessage;

    public HelpExecutor(final CMPlugin plugin) {
        this.plugin = plugin;
        this.refreshHelpMessage();
    }

    public void run(final CommandSender sender) {
        sender.sendMessage(
                Utils.colorize(this.helpMessage, false)
        );
    }

    public void refreshHelpMessage() {
        final Messages messages = this.plugin.getProvider().getMessages();

        this.helpMessage = "&0&m---------------------\n" +
                '\n' +
                messages.reloadUsage() + '\n' +
                messages.setupUsage() + '\n' +
                messages.passwordUsage() + '\n' +
                messages.historyUsage() + '\n' +
                '\n' +
                "&0&m---------------------";
    }
}

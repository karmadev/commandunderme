/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.spigot.command.sub.executors;

import es.karmadev.commandunderme.api.plugin.Config;
import es.karmadev.commandunderme.api.plugin.Messages;
import es.karmadev.commandunderme.plugin.spigot.CMPlugin;
import es.karmadev.commandunderme.api.util.Utils;
import es.karmadev.commandunderme.plugin.spigot.command.sub.CommandAnalyzer;
import org.bukkit.command.CommandSender;

import java.util.logging.Level;

public final class ReloadExecutor {

    private final CommandAnalyzer analyzer;
    private final CMPlugin plugin;

    public ReloadExecutor(final CommandAnalyzer analyzer, final CMPlugin plugin) {
        this.analyzer = analyzer;
        this.plugin = plugin;
    }

    public void run(final CommandSender sender) {
        final Config config = this.plugin.getProvider().getConfiguration();
        final Messages messages = this.plugin.getProvider().getMessages();

        if (!sender.hasPermission("cm.reload")) {
            sender.sendMessage(
                    Utils.colorize(messages.permission("cm.reload"), true)
            );
            return;
        }

        try {
            config.reload();
            messages.reload();

            this.analyzer.refreshHelpMessage();
        } catch (Throwable ex) {
            this.plugin.getLogger().log(Level.SEVERE, ex, () -> "Failed to reload plugin files");

            sender.sendMessage(Utils.colorize(messages.reloadFailure(), true));
            return;
        }

        sender.sendMessage(Utils.colorize(messages.reloadSuccess(), true));
        this.plugin.getProvider().detectCommands();
    }
}

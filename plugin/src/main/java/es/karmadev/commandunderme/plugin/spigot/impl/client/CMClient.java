/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.spigot.impl.client;

import com.google.common.base.Preconditions;
import es.karmadev.api.kson.io.JsonWriter;
import es.karmadev.api.kson.processor.JsonSerializable;
import es.karmadev.api.kson.processor.construct.JsonConstructor;
import es.karmadev.api.kson.processor.construct.JsonParameter;
import es.karmadev.api.kson.processor.conversor.UUIDTransformer;
import es.karmadev.api.kson.processor.field.JsonElement;
import es.karmadev.api.kson.processor.field.JsonExclude;
import es.karmadev.api.kson.processor.field.Transformer;
import es.karmadev.commandunderme.api.client.Client;
import es.karmadev.commandunderme.api.client.command.CommandTicket;
import es.karmadev.commandunderme.api.client.command.history.CommandHistory;
import es.karmadev.commandunderme.plugin.spigot.CMPlugin;
import es.karmadev.commandunderme.plugin.spigot.impl.client.history.CMHistory;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@JsonSerializable
public final class CMClient implements Client<Player> {

    @JsonExclude
    private final CMPlugin plugin = JavaPlugin.getPlugin(CMPlugin.class);

    @JsonExclude
    private Player player;

    @JsonElement(name = "username")
    private final String name;

    @JsonElement(name = "uniqueId")
    @Transformer(transformer = UUIDTransformer.class)
    private final UUID uniqueId;

    @JsonElement(name = "history")
    private final CMHistory history;

    @JsonExclude
    private CommandTicket ticket;

    @JsonConstructor
    public CMClient(final @JsonParameter(readFrom = "username") String name,
                    final @JsonParameter(readFrom = "uniqueId") UUID uniqueId,
                    final @JsonParameter(readFrom = "history") CMHistory history) {
        this.player = Bukkit.getPlayer(uniqueId);
        this.name = name;
        this.uniqueId = uniqueId;
        this.history = history;
    }

    /**
     * Load the player into the
     * client
     *
     * @param player the player to load
     */
    public void load(final Player player) {
        this.player = player;
    }

    /**
     * Get the client name
     *
     * @return the client name
     */
    @Override
    public @NotNull String getName() {
        return this.name;
    }

    /**
     * Get the client unique id
     *
     * @return the unique ID
     */
    @Override
    public @NotNull UUID getUniqueId() {
        return this.uniqueId;
    }

    /**
     * Get the current client command
     * token. The only way a client
     * might not have a ticket is when
     * he joins the game
     *
     * @return the command ticket
     */
    @Override
    public @NotNull Optional<CommandTicket> getCommandTicket() {
        return Optional.ofNullable(this.ticket);
    }

    /**
     * Set the current command
     * ticket
     *
     * @param ticket the client command ticket
     */
    public void setTicket(final CommandTicket ticket) {
        if (this.player == null)
            return;

        this.ticket = ticket;
    }

    /**
     * Get the client command
     * history
     *
     * @return the command history
     */
    @Override
    public @NotNull CommandHistory getHistory() {
        return this.history;
    }

    /**
     * Send a message to the client
     *
     * @param message the message to send
     * @param params  the messages parameters
     * @throws NullPointerException if the message is null
     */
    @Override
    public void sendMessage(final String message, final Object... params) throws NullPointerException {
        if (this.isOffline())
            return;

        Preconditions.checkNotNull(message, "Message cannot be null");
        String formatted = this.format(message, params);

        this.player.sendMessage(
                ChatColor.translateAlternateColorCodes('&', formatted)
        );
    }

    /**
     * Kick the client
     *
     * @param reason the kick reason
     * @param params the message parameters
     * @throws NullPointerException if the kick reason is null
     */
    @Override
    public void kick(final String reason, final Object... params) throws NullPointerException {
        if (this.isOffline())
            return;

        Preconditions.checkNotNull(reason, "Kick reason cannot be null");
        String formatted = this.format(reason, params);

        this.player.kickPlayer(
                ChatColor.translateAlternateColorCodes('&', formatted)
        );
    }

    /**
     * Tries to obtain the online
     * player of the client
     *
     * @return the client
     */
    @Override
    public @Nullable Player getPlayer() {
        return this.player;
    }

    /**
     * Get if the client is currently
     * online
     *
     * @return if the client is online
     */
    @Override
    public boolean isOffline() {
        return this.player == null ||
                !this.player.isOnline();
    }

    /**
     * Save the client
     * @throws IOException if the file fails to save
     */
    @Override
    public void save() throws IOException {
        File clientsFolder = new File(plugin.getDataFolder(), "clients");
        if (!clientsFolder.exists())
            if (!clientsFolder.mkdirs()) {
                plugin.getLogger().warning("Failed to create clients directory");
                return;
            }

        File clientFile = new File(clientsFolder, this.uniqueId.toString().replace("-", "") + ".json");
        if (!clientFile.exists())
            if (!clientFile.createNewFile()) {
                plugin.getLogger().warning("Failed to create client data file");
                return;
            }

        try (FileWriter writer = new FileWriter(clientFile)) {
            JsonWriter wr = new JsonWriter(this);
            wr.export(writer);
            writer.flush();
        }
    }

    /**
     * Get if the client can access
     * other client's history. This method
     * only works if both clients are
     * online
     *
     * @param other the other client
     * @return if the client can access the
     * other client history
     * @throws NullPointerException if the client to check
     *                              with is null
     */
    @Override
    public boolean canAccess(final Client<Player> other) throws NullPointerException {
        Preconditions.checkNotNull(other, "Client cannot be null");
        if (this.equals(other))
            return true;

        Player thisPlayer = this.getPlayer();
        Player otherPlayer = other.getPlayer();

        if (thisPlayer == null || otherPlayer == null)
            return false;

        thisPlayer.recalculatePermissions();
        otherPlayer.recalculatePermissions();

        Set<String> thisPermissions = getCapablePermissions(thisPlayer)
                .stream().filter((str) -> str.matches("^cm\\.privilege\\.[1-9]+[0-9]?$"))
                .collect(Collectors.toSet());
        Set<String> otherPermissions = getCapablePermissions(otherPlayer)
                .stream().filter((str) -> str.matches("^cm\\.privilege\\.[1-9]+[0-9]?$"))
                .collect(Collectors.toSet());

        if (thisPermissions.isEmpty())
            return otherPermissions.isEmpty();

        if (otherPermissions.isEmpty())
            return true;

        int thisLevel = getHighestLevel(thisPermissions);
        int otherLevel = getHighestLevel(otherPermissions);

        return thisLevel > otherLevel;
    }

    private String format(final String text, final Object... parameters) {
        if (parameters == null || parameters.length == 0)
            return text;

        StringBuilder builder = new StringBuilder(text);

        int index = builder.indexOf("{}");
        int paramIndex = 0;
        while (index != -1) {
            Object replacement = parameters[paramIndex++];
            builder.replace(index, index + 2, String.valueOf(replacement));

            if (paramIndex >= parameters.length)
                break;

            index = builder.indexOf("{}");
        }

        return builder.toString();
    }

    private static Set<String> getCapablePermissions(final Permissible permissible) {
        Set<String> permissions = new LinkedHashSet<>();

        Set<PermissionAttachmentInfo> thisPermissions = permissible.getEffectivePermissions();
        for (PermissionAttachmentInfo attachmentInfo : thisPermissions) {
            PermissionAttachment attachment = attachmentInfo.getAttachment();
            if (attachment != null)
                permissions.addAll(getCapablePermissions(permissible, attachment));

            String aPermission = attachmentInfo.getPermission();
            if (permissible.hasPermission(aPermission))
                permissions.add(attachmentInfo.getPermission());
        }

        return permissions;
    }

    private static Set<String> getCapablePermissions(final Permissible permissible, final PermissionAttachment attachment) {
        Set<String> permissions = new LinkedHashSet<>();

        Map<String, Boolean> permissionMap = attachment.getPermissions();
        for (String permission : permissionMap.keySet()) {
            if (permissible.hasPermission(permission))
                permissions.add(permission);
        }

        return permissions;
    }

    private static int getHighestLevel(final Collection<String> permissions) {
        int level = 0;
        for (String str : permissions) {
            int newLevel = getPermissionLevel(str);
            if (newLevel > level)
                level = newLevel;
        }

        return level;
    }

    private static int getPermissionLevel(final String permission) {
        if (permission == null || !permission.contains("."))
            return 0;

        String[] data = permission.split("\\.");
        if (data.length < 3)
            return 0;

        String significantData = data[2];
        try {
            return Integer.parseInt(significantData);
        } catch (NumberFormatException ignored) {}
        return 0;
    }
}

/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.spigot;

import es.karmadev.commandunderme.api.client.Client;
import es.karmadev.commandunderme.api.command.ProtectedCommand;
import es.karmadev.commandunderme.api.plugin.PluginWrapper;
import es.karmadev.commandunderme.plugin.spigot.command.CMCommand;
import es.karmadev.commandunderme.plugin.spigot.listener.CommandHandler;
import es.karmadev.commandunderme.plugin.spigot.listener.CompletionHandler;
import es.karmadev.commandunderme.plugin.spigot.listener.ConnectionHandler;
import es.karmadev.commandunderme.plugin.spigot.listener.adapter.TabCompleteAdapter;
import es.karmadev.commandunderme.api.util.DiscordWebhook;
import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.awt.*;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;

public class CMPlugin extends JavaPlugin {

    private CMProvider provider;

    @Override
    public void onEnable() {
        this.provider = new CMProvider(this);
        this.provider.loadClients();

        setupCommands();
        registerEvents();

        try {
            TabCompleteAdapter.register(this);
        } catch (NoClassDefFoundError ignored) {}
    }

    private void setupCommands() {
        PluginCommand cmCommand = this.getCommand("cm");
        if (cmCommand == null) {
            this.getLogger().warning("Failed to register \"cm\" command. Plugin won't initialize");
            return;
        }

        cmCommand.setExecutor(new CMCommand(this));
        this.provider.detectCommands();
    }

    private void registerEvents() {
        PluginManager manager = this.getServer().getPluginManager();

        manager.registerEvents(new ConnectionHandler(this), this);
        manager.registerEvents(new CommandHandler(this), this);

        String version = Bukkit.getVersion();
        if (version.matches(".*1\\.(7|8|9|10|11|12).*")) return;

        manager.registerEvents(new CompletionHandler(this), this);
    }

    public CMProvider getProvider() {
        return this.provider;
    }

    public void requestWebhook(final Client<Player> client, final boolean status, final ProtectedCommand command, final String[] arguments) {
        String url = this.provider.getConfiguration().getWebhookURL();
        if (url.trim().isEmpty())
            return;

        DiscordWebhook webhook = new DiscordWebhook(url);
        webhook.setUsername("CommandUnderMe");
        webhook.setAvatarUrl("https://static.wikia.nocookie.net/minecraft_gamepedia/images/c/ca/Milk_Bucket_JE2_BE2.png");

        DiscordWebhook.EmbedObject commandData = new DiscordWebhook.EmbedObject()
                .setTitle("New command execution")
                .setDescription("Minecraft plugin by [KarmaDev](https://github.com/KarmaDeb)")
                .addField("Client", client.getName(), false)
                .addField("Command", command.getName(), true);

        Optional<? extends PluginWrapper<?>> plugin = command.getPlugin();
        if (plugin.isPresent()) {
            commandData.addField("Plugin", plugin.get().getName(), true);
        } else {
            commandData.emptyField();
        }

        commandData.emptyField()
                .addField("Raw command", command.getRaw(), true)
                .addField("Status", (status ? "Success" : "Failed"), true);

        if (arguments.length > 0) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < arguments.length; i++) {
                builder.append(arguments[i]);
                if (i != arguments.length - 1)
                    builder.append(" ");
            }

            commandData.addField("Arguments", builder.toString(), false);
        }

        commandData.setFooter("This message was generated automatically", "");
        if (status) {
            commandData.setColor(Color.GREEN);
        } else {
            commandData.setColor(Color.RED);
        }

        webhook.addEmbed(commandData);
        try {
            webhook.execute();
        } catch (IOException ex) {
            this.getLogger().log(Level.SEVERE, "Tried to log to webhook but an exception occurred", ex);
        }
    }
}

/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.commandunderme.plugin.spigot.listener;

import es.karmadev.commandunderme.api.client.command.CommandTicket;
import es.karmadev.commandunderme.api.client.command.exception.ResultCompletedException;
import es.karmadev.commandunderme.api.client.command.history.CommandHistory;
import es.karmadev.commandunderme.api.command.CommandProcessor;
import es.karmadev.commandunderme.api.command.ProtectedCommand;
import es.karmadev.commandunderme.api.event.bukkit.command.CommandFailedEvent;
import es.karmadev.commandunderme.api.event.bukkit.command.CommandSuccessEvent;
import es.karmadev.commandunderme.api.plugin.Config;
import es.karmadev.commandunderme.api.plugin.Messages;
import es.karmadev.commandunderme.api.plugin.PluginWrapper;
import es.karmadev.commandunderme.plugin.spigot.CMPlugin;
import es.karmadev.commandunderme.plugin.spigot.impl.client.CMClient;
import es.karmadev.commandunderme.plugin.spigot.impl.client.history.CMEntry;
import es.karmadev.commandunderme.api.util.Utils;
import net.wesjd.anvilgui.AnvilGUI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.stream.Collectors;

public final class CommandHandler implements Listener {

    private final CMPlugin plugin;

    public CommandHandler(final CMPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onCommand(PlayerCommandPreprocessEvent e) {
        Config config = plugin.getProvider().getConfiguration();
        Player player = e.getPlayer();
        CMClient client = plugin.getProvider().getClient(player.getUniqueId())
                .orElseThrow(RuntimeException::new);

        String command = e.getMessage();
        if (handleHiddenCommand(player, config, plugin.getProvider().getMessages(), command, e))
            return;

        CommandTicket currentTicket = client.getCommandTicket().orElse(null);
        if (isConcurrentCommandAttempt(currentTicket)) {
            logAndCancel(player, e);
            return;
        }

        ProtectedCommand protectedCommand = Utils.findProtectedCommand(command);
        if (protectedCommand == null) return;

        if (currentTicket != null && handleExistingTicket(currentTicket, protectedCommand, config, client)) {
            logCommandExecution(true, command, player, client,
                    client.getCommandTicket().orElseThrow(RuntimeException::new), false);
            return;
        }

        initiateNewCommandFlow(player, command, client, protectedCommand);
        e.setCancelled(true); // Prevent execution until password is verified
    }

    private boolean handleHiddenCommand(final Player player, final Config config,
                                        final Messages messages, final String command,
                                        final PlayerCommandPreprocessEvent event) {
        String cmd = CommandProcessor.getCommandName(command);
        String plugin = CommandProcessor.getCommandPlugin(command);

        Collection<String> hidden = config.getHiddenCommands();
        for (String hiddenCMD : hidden) {
            String hCMD = CommandProcessor.getCommandName(hiddenCMD);
            String hPlugin = CommandProcessor.getCommandPlugin(hiddenCMD);

            if (notEvaluates(cmd, plugin, hCMD, hPlugin))
                continue;

            if (player.hasPermission("cm.execute"))
                return true;

            player.sendMessage(Utils.colorize(
                    messages.executionPermission(),
                    true
            ));
            event.setCancelled(true);
            return true;
        }

        return false;
    }

    private boolean isConcurrentCommandAttempt(final CommandTicket currentTicket) {
        return currentTicket != null && !currentTicket.isDefined();
    }

    private void logAndCancel(final Player player, final PlayerCommandPreprocessEvent e) {
        plugin.getLogger().warning(
                String.format("Client %s tried to run more than one command at once", player.getName())
        );
        e.setCancelled(true);
    }

    private boolean handleExistingTicket(final CommandTicket currentTicket, final ProtectedCommand protectedCommand,
                                         final Config config, final CMClient client) {
        if (currentTicket.isSuccess()) {
            switch (config.getProtectionMode()) {
                case CHAINED:
                    if (isWithinChainTime(currentTicket, config)) {
                        updateTicket(currentTicket, protectedCommand, true, client);
                        return true;
                    }
                    break;
                case TRUSTED:
                    updateTicket(currentTicket, protectedCommand, false, client);
                    return true;
                default:
                    break;
            }
        }

        return false;
    }

    private boolean isWithinChainTime(final CommandTicket currentTicket, final Config config) {
        Instant now = Instant.now();
        Instant last = currentTicket.getResultTime();
        return last != null && !now.isAfter(last.plusSeconds(config.getChainTime()));
    }

    private void updateTicket(final CommandTicket currentTicket, final ProtectedCommand protectedCommand, final boolean renew, final CMClient client) {
        CommandTicket updatedTicket = currentTicket.atCommand(protectedCommand, renew);
        client.setTicket(updatedTicket);
    }

    private void initiateNewCommandFlow(final Player player, final String command, final CMClient client, final ProtectedCommand protectedCommand) {
        CommandTicket newTicket = new CommandTicket(protectedCommand);
        client.setTicket(newTicket);

        if (plugin.getProvider().getCrypto().isSetup()) {
            ItemStack paper = createAnvilItem();
            new AnvilGUI.Builder()
                    .allowConcurrentClickHandlerExecution()
                    .plugin(plugin)
                    .title(ChatColor.BLUE + "Type password")
                    .itemLeft(paper)
                    .onClickAsync((slot, state) -> handleAnvilClickAsync(slot, state, newTicket, client, protectedCommand, command, player))
                    .onClose((state) -> {
                        try {
                            newTicket.setResult(false);
                            player.performCommand(String.valueOf(ThreadLocalRandom.current().nextInt()));
                        } catch (ResultCompletedException ignored) {}
                    })
                    .open(player);
            return;
        }

        handleResponseActions(newTicket, client, protectedCommand, command, player, "");
    }

    private ItemStack createAnvilItem() {
        ItemStack paper = new ItemStack(Material.PAPER);
        ItemMeta meta = paper.getItemMeta();
        if (meta != null) {
            meta.setDisplayName(" ");
            paper.setItemMeta(meta);
        }
        return paper;
    }

    private CompletableFuture<List<AnvilGUI.ResponseAction>> handleAnvilClickAsync(final Integer slot, final AnvilGUI.StateSnapshot state, final CommandTicket ticket,
                                                                                   final CMClient client, final ProtectedCommand protectedCommand,
                                                                                   final String command, final Player player) {
        return CompletableFuture.supplyAsync(() -> {
            if (slot == null)
                return Collections.emptyList();

            String input = Optional.ofNullable(state.getText()).orElse("").trim();
            if (ticket.isDefined()) return Collections.singletonList(AnvilGUI.ResponseAction.close());

            return handleResponseActions(ticket, client, protectedCommand, command, player, input);
        });
    }

    private @NotNull List<AnvilGUI.ResponseAction> handleResponseActions(CommandTicket ticket, CMClient client, ProtectedCommand protectedCommand,
                                                                         String command, Player player, String input) {
        boolean passwordValid = !plugin.getProvider().getCrypto().isSetup() || plugin.getProvider().getCrypto().validatePassword(input);
        processPasswordValidationResult(passwordValid, client, protectedCommand, ticket, command, player);

        return Collections.singletonList(AnvilGUI.ResponseAction.close());
    }

    private void processPasswordValidationResult(final boolean passwordValid, final CMClient client, final ProtectedCommand protectedCommand,
                                                 final CommandTicket ticket, final String command, final Player player) {
        try {
            Result result = determineResult(passwordValid, client, protectedCommand);
            Bukkit.getServer().getPluginManager().callEvent(result.eventToFire);
            ticket.setResult(result.success);

            logCommandExecution(result.success, command, player, client, ticket, true);
            client.save();
        } catch (IOException ex) {
            plugin.getLogger().log(Level.SEVERE, "Failed to store user " + player.getName(), ex);
        } catch (ResultCompletedException ignored) {}
    }

    private Result determineResult(boolean passwordValid, CMClient client, ProtectedCommand protectedCommand) {
        if (passwordValid) {
            CommandSuccessEvent event = new CommandSuccessEvent(client, protectedCommand);
            if (event.isCancelled()) {
                return new Result(false, new CommandFailedEvent(client, protectedCommand));
            }
            return new Result(true, event);
        }
        return new Result(false, new CommandFailedEvent(client, protectedCommand));
    }

    private void logCommandExecution(final boolean success, final String command, final Player player,
                                     final CMClient client, final CommandTicket ticket, final boolean execute) {
        String[] arguments = extractCommandArguments(command).toArray(new String[0]);

        Bukkit.getScheduler().runTask(this.plugin, () -> {
            Messages messages = this.plugin.getProvider().getMessages();
            String message;
            if (success) {
                if (execute) {
                    if (player.hasPermission("cm.execute")) {
                        player.performCommand(command.substring(1));
                    } else {
                        player.sendMessage(Utils.colorize(
                                messages.permission("cm.execute"),
                                true
                        ));
                    }
                }
                message = messages.successBroadcast(client.getName(), ticket.getCommand().getRaw());
            } else {
                if (execute) player.performCommand(String.valueOf(ThreadLocalRandom.current().nextInt()));
                message = messages.failureBroadcast(client.getName(), ticket.getCommand().getRaw());
            }

            Bukkit.getOnlinePlayers().stream().filter((cl) -> !cl.equals(player) && cl.hasPermission("cm.broadcast"))
                    .forEach((cl) -> cl.sendMessage(Utils.colorize(message, true)));
            Bukkit.getConsoleSender().sendMessage(Utils.colorize(message, true));

            this.plugin.requestWebhook(client, success, ticket.getCommand(), arguments);
        });

        CommandHistory history = client.getHistory();
        CMEntry entry = new CMEntry(Instant.now(), ticket.getResultTime(),
                ticket.getCommand().getName(),
                ticket.getCommand().getPlugin().map(PluginWrapper::getName).orElse(
                        ticket.getCommand().isBukkit() ? "Bukkit" : "<any>"
                ),
                ticket.isSuccess(),
                arguments);

        history.addEntry(entry);
    }

    private static class Result {
        public final boolean success;
        public final Event eventToFire;

        public Result(boolean success, Event eventToFire) {
            this.success = success;
            this.eventToFire = eventToFire;
        }
    }

    private static @NotNull List<String> extractCommandArguments(final String command) {
        if (command.contains(" ")) {
            return Arrays.stream(command.split(" "))
                    .skip(1)
                    .filter(str -> !str.trim().isEmpty())
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    private static boolean notEvaluates(final String source, final String srcPlugin,
                                        final String target, final String trgPlugin) {
        if (trgPlugin != null && srcPlugin == null)
            return true;

        if (srcPlugin != null && trgPlugin != null &&
                !trgPlugin.equalsIgnoreCase(srcPlugin))
            return true;

        return !source.equalsIgnoreCase(target);
    }
}